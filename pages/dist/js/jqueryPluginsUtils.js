/***
 * 設定jquery validator自訂的驗證方法
 */
function settingCustomValidator() {
    //  自訂表單驗證方法
    $.validator.addMethod("checkContentSpace",function(value,element,params){
        let checkTrimLength = value.trim().length;
        return this.optional(element)||checkTrimLength>0;
    },"<i class=\"icon fas fa-exclamation-triangle mr-1\"></i>請輸入空白以外的內容");

    $.validator.addMethod("checkName",function(value,element,params){
        let checkName = /^[A-Za-z0-9_-]{2,20}$/g;
        return this.optional(element)||(checkName.test(value));
    },"<i class=\"icon fas fa-exclamation-triangle mr-1\"></i>請輸入2~20位英文字母、數字、下底線或連字號");

    $.validator.addMethod("checkPassword",function(value,element,params){
        let checkName = /^[A-Za-z0-9_-`~!@#$%^&*()+=\\|{}\[\]"':;?/><.,]{8,20}$/g;
        return this.optional(element)||(checkName.test(value));
    },"<i class=\"icon fas fa-exclamation-triangle mr-1\"></i>請輸入8~20位英文字母、數字或符號");
}

/**
 * 設定DataTable自訂的排序方法
 */
function settingDataTableOrder() {
    // type1 : 去除html標記、空白，保留數字、小數點與負號
    $.fn.dataTable.ext.type.order['custom_order_type1-pre'] = function ( html_content ) {
        let bet_amount = html_content.replace(/<[\s\S]*?>/g, "").replace(/&amp;nbsp;/ig, "").replaceAll(/[^0-9.\-]/ig, '').trim();
        return bet_amount === '-' ? Number.MAX_SAFE_INTEGER : parseFloat(bet_amount);
    };

    // type2 : 先判斷是否有包含'text-danger'的class，若有表示為下降，去除html標記、空白，保留數字與小數點後再轉為負值
    $.fn.dataTable.ext.type.order['custom_order_type2-pre'] = function ( html_content ) {
        let bet_amount_percent;
        if(html_content.indexOf("text-danger") !== -1 ){
            bet_amount_percent = 0 - html_content.replace(/<[\s\S]*?>/g, "").replace(/&amp;nbsp;/ig, "").replaceAll(/[^0-9.]/ig, '').trim();
        }else{
            bet_amount_percent = html_content.replace(/<[\s\S]*?>/g, "").replace(/&amp;nbsp;/ig, "").replaceAll(/[^0-9.]/ig, '').trim();
        }
        return bet_amount_percent === '-' ? Number.MIN_SAFE_INTEGER : parseFloat(bet_amount_percent);
    };

    // type3 : split by '<br>' 後取第一個陣列數值，去除html標記、空白，保留數字、小數點與負號
    $.fn.dataTable.ext.type.order['custom_order_type3-pre'] = function ( html_content ) {
        let order_number_html = html_content.split('<br>')[0];
        let order_number = order_number_html.replace(/<[\s\S]*?>/g, "").replace(/&amp;nbsp;/ig, "").replaceAll(/[^0-9.\-]/ig, '').trim();
        return parseFloat(order_number);
    };
}

/**
 * 清空data table資料，避免memory leak(table必須帶有'dataTable'的class才會被清除)
 */
function removeDataTable() {
    $('body table.dataTable').DataTable().clear();  // 移除DataTable資料
    $('body table.dataTable').DataTable().destroy();  //銷毀原有的DataTable物件
}

/**
 * 清空data table資料，避免memory leak(by table id)
 * @param {string} element_id table id
 * @param {boolean} remove_thead
 */
function removeDataTableById(element_id, remove_thead = false) {
    // 先判斷表格是否已經被dataTable初始化過，若不是則不進行destroy，避免header寬度跑掉
    if($.fn.DataTable.isDataTable( '#' + element_id )){
        $('#' + element_id).DataTable().clear();  // 移除DataTable資料
        $('#' + element_id).DataTable().destroy();  //銷毀原有的DataTable物件
    }
    $('#' + element_id+ ' thead input[type="checkbox"]').prop('checked', true);  // 若表頭有checkbox則將其打勾
    if(remove_thead){
        $('#' + element_id+ ' thead').remove();  // 移除表頭
    }
    $('#' + element_id+ ' tbody').remove();  // 移除表頭以外的結果，避免重複
}

/**
 * 清空datatable紀錄的localStorage狀態(頁數、排序欄位)
 * @param {string[]} table_id_ary 要清除的table id陣列
 */
function clear_dataTable_localStorage(table_id_ary){
    if(table_id_ary !== undefined){
        for(let i=0 ; i<table_id_ary.length ; i++){
            localStorage.removeItem(table_id_ary[i]);
        }
    }else{
        localStorage.clear();
    }
}

/**
 * 清除dateRangePicker物件
 */
function removeDateRangePicker(parent_element_id = '') {
    if(parent_element_id !== ''){
        $('#' + parent_element_id + ' .daterangepicker').remove();
    }else{
        $('.daterangepicker').remove();
    }
}

/**
 * 顯示成功toast
 * @param {string} message 要顯示的文字
 */
function showSuccessToast(message){
    toastr.success('', message);
}

/**
 * 顯示失敗toast
 * @param {string} message 要顯示的文字
 */
function showFailToast(message){
    toastr.error('', message);
}

/**
 * 將swal2的確認按鈕改為loading圖示
 */
function showLoadingSwal2Btn() {
    $('.swal2-confirm.btn').removeClass('btn-success').addClass('pt-0 pb-0').html('<img src="./dist/img/loading.gif" class="img-size-36 m-auto">');
}

/**
 * 恢復swal2的確認按鈕
 */
function resetSwal2ConfirmBtn() {
    $('.swal2-confirm.btn').addClass('btn-success').removeClass('pt-0 pb-0').html('確認');
}

/**
 * 清空已存在memory中的chart.js物件，避免memory leak
 */
function destroyChartJS() {
    for(let key in window.ChartWindowVarAry){
        window.ChartWindowVarAry[key].destroy();
        window.ChartWindowVarAry[key] = null;
        delete window.ChartWindowVarAry[key];
    }
    window.ChartWindowVarAry = [];
}

/**
 * 清空已存在memory中的chart.js物件，避免memory leak(針對modal或popup window)
 */
function destroyChartJS_modal_popup(page_name) {
    for(let key in window.ChartWindowVarAry_modal_popup[page_name]){
        window.ChartWindowVarAry_modal_popup[page_name][key].destroy();
        window.ChartWindowVarAry_modal_popup[page_name][key] = null;
        delete window.ChartWindowVarAry_modal_popup[page_name][key];
    }
    delete window.ChartWindowVarAry_modal_popup[page_name];
}

/**
 * 清空已存在memory中的HighChart物件，避免memory leak
 */
function destroyHighChart() {
    for(let key in window.HighChartWindowVarAry){
        window.HighChartWindowVarAry[key].destroy();
        window.HighChartWindowVarAry[key] = null;
        delete window.HighChartWindowVarAry[key];
    }
    window.HighChartWindowVarAry = [];
}

/**
 * 清空已存在memory中的HighChart物件，避免memory leak(針對modal或popup window)
 */
function destroyHighChart_modal_popup(page_name) {
    for(let key in window.HighChartWindowVarAry_modal_popup[page_name]){
        window.HighChartWindowVarAry_modal_popup[page_name][key].destroy();
        window.HighChartWindowVarAry_modal_popup[page_name][key] = null;
        delete window.HighChartWindowVarAry_modal_popup[page_name][key];
    }
    delete window.HighChartWindowVarAry_modal_popup[page_name];
}

/**
 * 替自訂的legend checkbox加上dataset代表的顏色icon
 * @param legend_container_entity legend容器物件
 * @param dataset_label 對應要新增的dataset文字
 * @param dataset_color 對應要新增的dataset顏色
 */
function addDatasetColorToCheckbox(legend_container_entity, dataset_label, dataset_color){
    let target_el = legend_container_entity.$('td:contains(' + dataset_label + ')');
    let legend_html = '<span class="text-md mr-2" style="color: ' + dataset_color + '"><i class="fab fa-deezer"></i></span>';
    $(target_el[0]).closest('tr').find('input[type="checkbox"]').closest('td').prepend(legend_html);
}

/**
 * chart.js 顯示/隱藏checkbox勾選的dataset
 * @param {Object} chart 要渲染的圖表物件
 * @param {Object} checkbox_container_entity checkbox存放的dom物件
 * @param {int} checkbox_mode checkbox模式 0:全選 1:單選
 * @param {boolean} is_checked checkbox勾選設定
 * @param {string} dataset_label 要渲染的圖表dataset label(單選模式才使用)
 */
function changeCheckboxDatasetDisplayStatus(chart, checkbox_container_entity, checkbox_mode, is_checked, dataset_label= '') {
    let ci = chart.chart;
    let all_element = $('#' + checkbox_container_entity[0].id).find('.checkbox_all');

    if(checkbox_mode === 0){
        //  全選checkbox的圖表渲染
        checkbox_container_entity.$('div[class!="checkbox_all"] input[type="checkbox"]').prop('checked', is_checked);  //  除了全選checkbox，渲染其他checkbox勾選狀態
        //  處理圖表渲染
        for(let i=0 ; i<ci.data.datasets.length ; i++){
            let meta = ci.getDatasetMeta(i);
            meta.hidden = !is_checked
        }
    }else if(checkbox_mode === 1){
        //  單選checkbox的圖表渲染
        for(let i=0 ; i<ci.data.datasets.length ; i++){
            let meta = ci.getDatasetMeta(i);
            if(ci.data.datasets[i].label === dataset_label.trim()){
                meta.hidden = !is_checked;
                break;
            }
        }

        //  判斷圖表是否已經全選，若是則將全選checkbox打勾，否則將全選checkbox取消打勾
        let is_show_all = true;

        //  若全選的checkbox存在才處理
        if(all_element){
            for(let i=0 ; i<ci.data.datasets.length ; i++){
                let meta = ci.getDatasetMeta(i);
                if(meta.hidden){
                    is_show_all = false;
                    break;
                }
            }
            //  渲染全選checkbox勾選狀態
            all_element.find('input[type="checkbox"]').prop('checked', is_show_all);
        }
    }
    ci.update();
}

/**
 * chart.js 保留點擊的dataset，顯示/隱藏其他dataset
 * @param {Object}chart
 * @param {Object}legendItem
 */
function changeDatasetDisplayStatus(chart, legendItem) {
    let index = legendItem.length !== 0 ? legendItem[0]._datasetIndex : -1;
    let ci = chart.chart;

    if(index !== -1){  // 判斷滑鼠點擊的部分是否為dataset的節點
        let is_all_hide;  // 紀錄目前dataset是否已為全部隱藏
        for(let i=0 ; i<ci.data.datasets.length ; i++){
            if (i !== index) {
                is_all_hide = false;
                let alreadyHidden = (ci.getDatasetMeta(i).hidden === null) ? false : ci.getDatasetMeta(i).hidden;
                if(alreadyHidden){
                    is_all_hide = true;
                }else{
                    break;
                }
            }
        }

        if(is_all_hide){
            ci.data.datasets.forEach(function(e, i) {
                let meta = ci.getDatasetMeta(i);
                if (i !== index) {
                    meta.hidden = null;
                }
            });
        }else{
            ci.data.datasets.forEach(function(e, i) {
                let meta = ci.getDatasetMeta(i);
                if (i !== index) {
                    meta.hidden = true;
                }
            });
        }
        ci.update();
    }
}

/**
 * 判斷chart點擊的位置是否為legend
 * @param {string} element_id  要判斷的element_id
 * @param {object} chart 要處理的chartJS物件
 * @param {object} event 點擊的物件
 * @return {boolean}
 */
function checkIsClickLegend(element_id, chart, event) {
    let box = chart.boxes[0];
    let chart_layout_padding = chart.options.layout.padding;  // 圖表padding設定
    let chart_clientX = $('#' + element_id).offset().left;  // 圖表位於畫面的X座標
    let chart_clientY = $('#' + element_id).offset().top - $(this).scrollTop();  // 圖表位於畫面的Y座標

    return ((box.position === "top" && event.clientY < chart_layout_padding.top + chart_clientY + box.height) ||
        (box.position === "right" && event.clientX > chart_clientX + chart.width - chart_layout_padding.right - box.width) ||
        (box.position === "bottom" && event.clientY > chart_clientY + chart.height - chart_layout_padding.bottom - box.height) ||
        (box.position === "left" && event.clientX < chart_clientX + chart_layout_padding.left + box.width));
}

/**
 * 顯示 chart.js datasets 資料數值於圖表上 (bar & line chart 使用此方法，pie & polar chart 使用 chartjs-plugin-labels)
 * @param {object} chart
 * @param {int} fontSize  文字大小
 * @param {int} datasetsShowedLimit  顯示的datasets數量上限，超過不顯示
 * @param {int} dataLength  要顯示的data數量上限，超過不顯示
 * @param {int} numberPrecision  要顯示的小數點位數
 */
function showDatasetsLabels(chart, fontSize = 12, datasetsShowedLimit = 5, dataLength = 20, numberPrecision = 0) {
    // Define a plugin to provide data labels
    let ctx = chart.ctx;
    let box = chart.boxes[0];
    let chart_layout_padding = chart.options.layout.padding;  // 圖表padding設定

    // dataset label添加方法
    let drawDatasetLabel = function(dataset, meta){
        let pre_data_object = {
            dataString_width: 0,
            position_x: 0,
            position_y: 0,
            textAlign: '',
            textBaseline: ''
        };
        if (!meta.hidden && meta.data.length <= dataLength) {  //  若資料少於設定的筆數才顯示label
            meta.data.forEach(function(element, index) {
                // Draw the text in black, with the specified font
                ctx.fillStyle = 'rgb(0, 0, 0)';

                let fontStyle = 'normal';
                ctx.font = Chart.helpers.fontString(fontSize, fontStyle);

                // Just naively convert to string for now
                let dataString = FormatNumber(dataset.data[index], '', numberPrecision).toString();
                let dataString_width = ctx.measureText(dataString).width;  //  取得該點資料字串寬度

                // Make sure alignment settings are correct
                ctx.textAlign = 'center';
                ctx.textBaseline = 'bottom';

                let position = element.tooltipPosition();
                let position_x = position.x;
                let position_y = position.y;
                let checkIsOverlap = function () {  //  判斷資料點文字是否有重疊
                    if(ctx.textBaseline !== pre_data_object.textBaseline){
                        //  若文字baseline不同，兩點之間的高度為文字大小的兩倍距離才能確保不會重疊
                        return position_x - dataString_width / 2 < pre_data_object.position_x + pre_data_object.dataString_width / 2 && Math.abs(position_y - pre_data_object.position_y) < fontSize * 2
                    }else{
                        return position_x - dataString_width / 2 < pre_data_object.position_x + pre_data_object.dataString_width / 2 && Math.abs(position_y - pre_data_object.position_y) < fontSize
                    }
                };
                let checkIsExceedTopBorder = function () {  //  判斷資料點文字是否有超過圖表頂端邊界
                    return position_y - fontSize < 0;
                };
                let checkIsOverlapTopLegend = function () {  //  判斷資料點文字是否有重疊到上方legend
                    return box.position === "top" && position_y - fontSize < chart_layout_padding.top + box.height;
                };

                //  依據不同種類的圖表進行處理
                if(meta.type === 'horizontalBar'){
                    ctx.textAlign = 'left';
                    ctx.textBaseline = 'middle';
                    if(position_x + dataString_width > chart.width){  //  若文字超過邊界，調整文字對齊方式
                        ctx.textAlign = 'right';
                    }
                }else {
                    position_x = position_x + dataString_width / 2 < chart.width ? position_x : position_x - (position_x + dataString_width / 2 - chart.width);  //  若文字會超出canvas邊界，調整x軸偏移量
                    if(meta.type === 'bar'){
                        if(dataString.indexOf('-') === -1){
                            if(checkIsOverlap()){
                                position_y = position_y - fontSize * 2;
                                if(checkIsExceedTopBorder()){
                                    ctx.textBaseline = 'top';
                                    position_y = position_y + fontSize * 2;
                                    if(checkIsOverlap()){
                                        position_y = position_y + fontSize * 2;
                                    }
                                }
                            }else{
                                if(checkIsExceedTopBorder()){
                                    ctx.textBaseline = 'top';
                                    if(checkIsOverlap()){
                                        position_y = position_y + fontSize * 2;
                                    }
                                }
                            }
                        }else{
                            ctx.textBaseline = 'top';
                            if(checkIsOverlap()){
                                position_y = position_y + fontSize * 2;
                            }
                        }
                    }else if(meta.type === 'line'){
                        position_y = position.y - fontSize / 2;
                        if(checkIsOverlap()){
                            if(position_y > pre_data_object.position_y){
                                ctx.textBaseline = 'top';
                                position_y = position_y + fontSize;
                            }else{
                                position_y = position_y - fontSize;

                                // 若legend在上方時，判斷文字是否會重疊到
                                if(checkIsOverlapTopLegend()){
                                    ctx.textBaseline = 'top';
                                    position_y = position_y + fontSize * 2;
                                    if(checkIsOverlap()){
                                        position_y = position_y + fontSize * 2;
                                    }
                                }
                            }
                        }else{
                            // 若legend在上方時，判斷文字是否會重疊到
                            if(checkIsOverlapTopLegend()){
                                ctx.textBaseline = 'top';
                                position_y = position_y + fontSize;
                                if(checkIsOverlap()){
                                    position_y = position_y + fontSize * 2;
                                }
                            }
                        }
                    }
                }
                ctx.fillText(dataString, position_x, position_y);

                //  紀錄當前資料座標供下個資料判斷使用
                pre_data_object.dataString_width = dataString_width;
                pre_data_object.position_x = position_x;
                pre_data_object.position_y = position_y;
                pre_data_object.textAlign = ctx.textAlign;
                pre_data_object.textBaseline = ctx.textBaseline;
            });
        }
    };

    let datasets_showed_idx_ary = [];  //  紀錄目前顯示的dataset index
    chart.data.datasets.forEach(function (dataset, i) {
        let meta = chart.getDatasetMeta(i);
        if(!meta.hidden){
            datasets_showed_idx_ary.push(i);
        }
    });

    //  若datasets canvas上顯示的數量小於等於設定數量才添加label
    if(datasets_showed_idx_ary.length <= datasetsShowedLimit){
        for(let i=0 ; i<datasets_showed_idx_ary.length ; i++){
            drawDatasetLabel(chart.data.datasets[datasets_showed_idx_ary[i]], chart.getDatasetMeta(datasets_showed_idx_ary[i]));
        }
    }
}