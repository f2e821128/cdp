// Date range picker 固定設定
const date_ranges_quick_select_config_1 = {
    '近1週': [moment().subtract(7, 'day'), moment()],
    '近2週': [moment().subtract(14, 'day'), moment()],
    '近1個月': [moment().add(1, 'day').subtract(1, 'month'), moment()],
    '近2個月': [moment().add(1, 'day').subtract(2, 'month'), moment()],
    '近3個月': [moment().add(1, 'day').subtract(3, 'month'), moment()],
    '近半年': [moment().add(1, 'day').subtract(6, 'month'), moment()],
    '近1年': [moment().add(1, 'day').subtract(12, 'month'), moment()]
    //'這個月': [moment().startOf('month'), moment().endOf('month')],
    //'上個月': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
};

const date_ranges_quick_select_config_2 = {
    '近2週': [moment().subtract(14, 'day'), moment()],
    '近1個月': [moment().add(1, 'day').subtract(1, 'month'), moment()],
    '近2個月': [moment().add(1, 'day').subtract(2, 'month'), moment()],
    '未來2週': [moment().add(1, 'day'), moment().add(14, 'day')],
    '未來1個月': [moment().add(1, 'day'), moment().add(1, 'month')],
    '未來2個月': [moment().add(1, 'day'), moment().add(2, 'month')]
};

const date_locale_config = {
    format: 'YYYY-MM-DD',  //日期顯示格式
    separator: ' ~ ',  //日期區間分隔符號
    customRangeLabel: "自訂日期區間",
    daysOfWeek: ["日","一","二","三","四","五","六"],
    monthNames: ["January","February","March","April","May","June","July","August","September","October","November","December"]
};

// config_1 : 預設選取近1個月，最早可選至3年前，最晚可選至前一日
const date_range_picker_config_1 = {
    autoApply: true,  //點選後是否自動應用日期
    startDate: moment().add(1, 'day').subtract(1, 'month'),  //預設起始時間
    endDate: moment().startOf('day').subtract(1, 'day'),  //預設結束時間
    minDate : moment().add(1, 'day').subtract(3, 'year'),  //限制最小可選日期
    maxDate : moment().startOf('day').subtract(1, 'day'),  //限制最大可選日期
    ranges: date_ranges_quick_select_config_1,
    locale: date_locale_config,
    showDropdowns: true,
    linkedCalendars: false
};

// config_2 : 預設選取近20年，最早可選至20年前，最晚可選至前一日
const date_range_picker_config_2 = {
    autoApply: true,  //點選後是否自動應用日期
    startDate: moment().add(1, 'day').subtract(20, 'year'),  //預設起始時間
    endDate: moment().startOf('day').subtract(1, 'day'),  //預設結束時間
    minDate : moment().add(1, 'day').subtract(20, 'year'),  //限制最小可選日期
    maxDate : moment().startOf('day').subtract(1, 'day'),  //限制最大可選日期
    ranges: {
        '近1週': [moment().subtract(7, 'day'), moment()],
        '近2週': [moment().subtract(14, 'day'), moment()],
        '近1個月': [moment().add(1, 'day').subtract(1, 'month'), moment()],
        '近1年': [moment().add(1, 'day').subtract(1, 'year'), moment()],
        '近3年': [moment().add(1, 'day').subtract(3, 'year'), moment()],
        '近5年': [moment().add(1, 'day').subtract(5, 'year'), moment()],
        '近20年': [moment().add(1, 'day').subtract(20, 'year'), moment()]
    },
    locale: date_locale_config,
    showDropdowns: true,
    linkedCalendars: false
};

// config_3 : 預設選取未來2週，最早可選至3年前，最晚可選至1年後
const date_range_picker_config_3 = {
    autoApply: true,  //點選後是否自動應用日期
    startDate: moment(),  //預設起始時間
    endDate: moment().add(14, 'day'),  //預設結束時間
    minDate : moment().add(1, 'day').subtract(3, 'year'),  //限制最小可選日期
    maxDate : moment().startOf('day').add(1, 'year'),  //限制最大可選日期
    ranges: date_ranges_quick_select_config_2,
    locale: date_locale_config,
    showDropdowns: true,
    linkedCalendars: false
};

// config_4 : 單日期模式，預設選前2日，最早可選至3年前，最晚可選至前2日
const date_range_picker_config_4 = {
    autoApply: true,  //點選後是否自動應用日期
    singleDatePicker: true,  //是否為單日期模式
    startDate: moment().startOf('day').subtract(2, 'day'),  //預設起始時間
    endDate: moment().startOf('day').subtract(2, 'day'),  //預設結束時間
    minDate: moment().add(1, 'day').subtract(3, 'year'),  //限制最小可選日期
    maxDate: moment().startOf('day').subtract(2, 'day'),  //限制最大可選日期
    locale: date_locale_config,
    showDropdowns: true,
    linkedCalendars: false
};

// config_5 : 預設選取近1個月，最早可選至3年前，最晚可選至前一日，無快速選單
const date_range_picker_config_5 = {
    autoApply: true,  //點選後是否自動應用日期
    startDate: moment().add(1, 'day').subtract(1, 'month'),  //預設起始時間
    endDate: moment().startOf('day').subtract(1, 'day'),  //預設結束時間
    minDate : moment().add(1, 'day').subtract(3, 'year'),  //限制最小可選日期
    maxDate : moment().startOf('day').subtract(1, 'day'),  //限制最大可選日期
    locale: date_locale_config,
    showDropdowns: true,
    linkedCalendars: false
};

// config_6 : 預設選取近2個月，最早可選至3年前，最晚可選至1年後
const date_range_picker_config_6 = {
    autoApply: true,  //點選後是否自動應用日期
    startDate: moment().add(1, 'day').subtract(2, 'month'),  //預設起始時間
    endDate: moment().startOf('day').add(0, 'day'),  //預設結束時間
    minDate : moment().add(1, 'day').subtract(3, 'year'),  //限制最小可選日期
    maxDate : moment().startOf('day').add(1, 'year'),  //限制最大可選日期
    ranges: date_ranges_quick_select_config_2,
    locale: date_locale_config,
    showDropdowns: true,
    linkedCalendars: false
};

// config_7 : 預設選取近一週，最早可選至3年前，最晚可選至前一日
const date_range_picker_config_7 = {
    autoApply: true,  //點選後是否自動應用日期
    startDate: moment().startOf('day').subtract(7, 'day'),  //預設起始時間
    endDate: moment().startOf('day').subtract(1, 'day'),  //預設結束時間
    minDate : moment().add(1, 'day').subtract(3, 'year'),  //限制最小可選日期
    maxDate : moment().startOf('day').subtract(1, 'day'),  //限制最大可選日期
    ranges: date_ranges_quick_select_config_1,
    locale: date_locale_config,
    showDropdowns: true,
    linkedCalendars: false
};

// config_8 : 預設選取近一週，最早可選至3年前，最晚可選至當日
const date_range_picker_config_8 = {
    autoApply: true,  //點選後是否自動應用日期
    startDate: moment().startOf('day').subtract(7, 'day'),  //預設起始時間
    endDate: moment().startOf('day').subtract(0, 'day'),  //預設結束時間
    minDate : moment().add(1, 'day').subtract(3, 'year'),  //限制最小可選日期
    maxDate : moment().startOf('day').subtract(0, 'day'),  //限制最大可選日期
    ranges: date_ranges_quick_select_config_1,
    locale: date_locale_config,
    showDropdowns: true,
    linkedCalendars: false
};

// config_9 : 預設選取近一個月，最早可選至3年前，最晚可選至前一日，一次最多只能選取三個月，無快速選單
const date_range_picker_config_9 = {
    autoApply: true,  //點選後是否自動應用日期
    startDate: moment().add(1, 'day').subtract(1, 'month'),  //預設起始時間
    endDate: moment().startOf('day').subtract(1, 'day'),  //預設結束時間
    minDate : moment().add(1, 'day').subtract(3, 'year'),  //限制最小可選日期
    maxDate : moment().startOf('day').subtract(1, 'day'),  //限制最大可選日期
    maxSpan: {
        months: 3
    },
    locale: date_locale_config,
    showDropdowns: true,
    linkedCalendars: false
};

// config_10 : 預設選取近一個月，最早可選至3年前，最晚可選至前一日，一次最多只能選取一年
const date_range_picker_config_10 = {
    autoApply: true,  //點選後是否自動應用日期
    startDate: moment().add(1, 'day').subtract(1, 'month'),  //預設起始時間
    endDate: moment().startOf('day').subtract(1, 'day'),  //預設結束時間
    minDate : moment().add(1, 'day').subtract(3, 'year'),  //限制最小可選日期
    maxDate : moment().startOf('day').subtract(1, 'day'),  //限制最大可選日期
    maxSpan: {
        year: 1
    },
    ranges: {
        '近1週': [moment().subtract(7, 'day'), moment().startOf('day').subtract(1, 'day')],
        '近2週': [moment().subtract(14, 'day'), moment().startOf('day').subtract(1, 'day')],
        '近1個月': [moment().add(1, 'day').subtract(1, 'month'), moment().startOf('day').subtract(1, 'day')],
        '近2個月': [moment().add(1, 'day').subtract(2, 'month'), moment().startOf('day').subtract(1, 'day')],
        '近3個月': [moment().add(1, 'day').subtract(3, 'month'), moment().startOf('day').subtract(1, 'day')],
        '近半年': [moment().add(1, 'day').subtract(6, 'month'), moment().startOf('day').subtract(1, 'day')],
        '近一年': [moment().add(1, 'day').subtract(1, 'year'), moment().startOf('day').subtract(1, 'day')],
        '近兩年': [moment().add(1, 'day').subtract(2, 'year'), moment().startOf('day').subtract(1, 'day')],
        '近三年': [moment().add(1, 'day').subtract(3, 'year'), moment().startOf('day').subtract(1, 'day')]
    },
    locale: date_locale_config,
    showDropdowns: true,
    linkedCalendars: false
};

// config_11 : 預設選取近1個月，最早可選至3年前，最晚可選至前二日
const date_range_picker_config_11 = {
    autoApply: true,  //點選後是否自動應用日期
    startDate: moment().add(1, 'day').subtract(1, 'month'),  //預設起始時間
    endDate: moment().startOf('day').subtract(2, 'day'),  //預設結束時間
    minDate : moment().add(1, 'day').subtract(3, 'year'),  //限制最小可選日期
    maxDate : moment().startOf('day').subtract(2, 'day'),  //限制最大可選日期
    ranges: date_ranges_quick_select_config_1,
    locale: date_locale_config,
    showDropdowns: true,
    linkedCalendars: false
};

// config_12 : 預設選取前後一個月，最早可選至20年前，最晚可選至一年後
const date_range_picker_config_12 = {
    autoApply: true,  //點選後是否自動應用日期
    startDate: moment().add(1, 'day').subtract(1, 'month'),  //預設起始時間
    endDate: moment().startOf('day').add(1, 'month'),  //預設結束時間
    minDate : moment().add(1, 'day').subtract(3, 'year'),  //限制最小可選日期
    maxDate : moment().startOf('day').add(1, 'year'),  //限制最大可選日期
    ranges: date_ranges_quick_select_config_2,
    locale: date_locale_config,
    showDropdowns: true,
    linkedCalendars: false
};

//  sweetAlert彈出視窗設定
const swal_config = {
    scrollbarPadding: false,
    icon: 'warning',
    iconColor: '#ffc107',
    confirmButtonText: '確認',
    showCancelButton: true,
    cancelButtonText: '取消',
    focusCancel: true,
    allowOutsideClick: false,
    buttonsStyling: false,
    reverseButtons: true,
    customClass: {
        popup: 'bg-gradient-light',
        icon: 'font-weight-bolder mt-0 mb-3 elevation-2',
        title: 'text-black text-lg',
        content: 'custom-content text-black',
        actions: 'custom-actions',
        confirmButton: 'btn btn-success float-right mt-3',
        cancelButton: 'btn btn-danger float-left mt-3',
    }
};
const swal_toast_config = {
    toast: true,
    scrollbarPadding: false,
    icon: 'info',
    iconColor: '#17a2b8',
    showConfirmButton: true,
    confirmButtonText: '確認',
    showCancelButton: false,
    cancelButtonText: '取消',
    buttonsStyling: false,
    reverseButtons: true,
    customClass: {
        popup: 'bg-gradient-light',
        icon: 'font-weight-bolder elevation-2',
        title: 'text-black',
        content: 'custom-content text-black',
        actions: 'custom-actions',
        confirmButton: 'btn btn-sm btn-success',
        cancelButton: 'btn btn-sm btn-danger mr-1',
    }
};

//  introJs設定
const introJs_config = {
    nextLabel: '->',
    prevLabel: '<-',
    doneLabel: '我知道了',
    exitOnOverlayClick: false,
    disableInteraction: true,
    hidePrev: true
};

//  toastr設定
const toastr_config = {
    "positionClass": "toast-top-right",
    "progressBar": true,
    "showDuration": "300",
    "hideDuration": "300",
    "timeOut": "3000",
    "extendedTimeOut": "1000",
    "showMethod": "slideDown",
    "hideMethod": "slideUp"
};

class CHART_ROWS_OBJECT {
    constructor(from, to) {
        this.from = from;
        this.to = to;
        this.weight = 1;
    }
    get strfrom() {
        return this.from;
    }
    get strto() {
        return this.to;
    }
    get intweight() {
        return this.weight;
    }
    addWeight(num) {
        this.weight += num;
    }
}