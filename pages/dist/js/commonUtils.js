/**
 * content區塊導頁方法
 * @param {string} folder_path 目錄路徑
 * @param {string} pageName 轉導頁面名稱
 * @param {boolean} isLoadingAutoHide loading視窗是否自動關閉
 * @param {boolean} isNeedClearPageRecord 是否需要清空頁面瀏覽歷程紀錄
 */
function redirectToPage(folder_path, pageName, isLoadingAutoHide = true, isNeedClearPageRecord = true){
    window.fromPage = 'from_page=' + pageName;  // 紀錄user trace log的item_id
    let path = 'pages/' + folder_path  + pageName + '.html';

    if(isNeedClearPageRecord){
        window.pageRecordStack = [];  // 清空頁面瀏覽紀錄
    }

    removeDateRangePicker();  // 清除dateRangePicker物件
    removeDataTable();  // 清除dataTable物件
    destroyChartJS();  // 清除chart.js物件
    destroyHighChart();  // 清除HighChart物件

    showLoadingModal();  // 顯示Loading視窗
    if(isLoadingAutoHide){
        setTimeout(function(){  // 等待指定秒數後關閉Loading視窗
            closeLoadingModal();
        }, 500);
    }

    $("#content").empty();  // 清空content dom內容
    $("#content").load(path + '?' + Date.now(), function(){
        // 設定頁面title
        let cur_page_icon = $('<div />').append($('#sidebar_menu .nav-link.active i:first-child').clone()).html();
        let cur_page_title = $('#sidebar_menu .nav-link.active').text();

        $('.page-title.table__title-text').before(cur_page_icon);
        $('.page-title').text(cur_page_title);

        // 若頁面總高度超過多少px，加上回到頂端的按鈕
        if(document.body.scrollHeight > 100){
            $("#content").append('<button type="button" id="gotop" class="toTop-arrow" onclick="goToTop();"></button>');
        }

        if($('body').hasClass('sidebar-open')){  // 若使用手機瀏覽，則導頁後將sidebar收合
            $('body').removeClass('sidebar-open').addClass('sidebar-collapse');
        }

        $(window).scrollTop('0');  // 將頁面回到頂端

        // 設定返回按鈕需導回的頁面
        //set_go_back_btn();
    });
}

/**
 * 將畫面返回到最頂上
 */
function goToTop(){
    $('html,body').animate({ scrollTop: 0 }, 0);
}

/**
 * 取得儲存在sessionStorage中的JSON物件
 * @param {string} key sessionStorage的key值
 * @return {object} sessionStorage儲存的JSON物件
 */
function getSessionStorageEntity(key) {
    return JSON.parse(sessionStorage.getItem(key));
}

/**
 * 將搜尋框中的參數存入sessionStorage，以便返回頁面時帶入(需帶入form的id array)
 * @param {string} page_name 要產生的sessionStorage變數名稱
 * @param {string[]} form_id_ary 需要序列化的form id
 */
function generate_search_param(page_name, form_id_ary){
    let searchParam = {};
    for(let i=0 ; i<form_id_ary.length ; i++){
        let form_input_ary = decodeURIComponent($("#" + form_id_ary[i]).serialize()).split('&');
        for(let j=0 ; j<form_input_ary.length ; j++){
            let input_ary = form_input_ary[j].split('=');
            searchParam[input_ary[0]] = input_ary[1];
        }
    }
    sessionStorage.setItem('searchParam_' + page_name, JSON.stringify(searchParam));
}

/**
 * 清空搜尋框需帶入的sessionStorage(若無指定page_name則將所有searchParam_開頭的變數皆清除)
 * @param {string[]} page_name_ary 要清除資料的頁面名稱
 */
function clear_search_param(page_name_ary= []){
    if(page_name_ary.length !== 0){
        for(let i=0 ; i<page_name_ary.length ; i++){
            let sessionStorage_key = 'searchParam_' + page_name_ary[i];
            sessionStorage.removeItem(sessionStorage_key);
        }
    }else{
        let remove_sessionStorage_ary = [];  // 紀錄要移除的sessionStorage名稱
        for(let i=0 ; i<sessionStorage.length ; i++){
            if(sessionStorage.key(i).substring(0, 11) === 'searchParam'){
                remove_sessionStorage_ary.push(sessionStorage.key(i));
            }
        }
        for(let i=0 ; i<remove_sessionStorage_ary.length ; i++){
            sessionStorage.removeItem(remove_sessionStorage_ary[i]);
        }
    }
}

/**
 * 將進階搜尋卡片展開
 */
function openSearchCard() {
    $('div.search-card').removeClass('collapsed-card');
    $('i.fa-plus').removeClass('fa-plus').addClass('fa-minus');
}

/**
 * 錯誤訊息通用顯示格式
 * @param {object} error 錯誤物件
 * @returns {string}
 */
function errorRespond(error){
    return error.return_code + ' : ' + error.message;
}

/**
 * 取得目前所在頁面名稱
 * @param url
 * @return {*|string}
 */
function getCurrentPageName(url) {
    let url_ary = url.split('/');
    return url_ary[url_ary.length - 1];
}

/**
 * 將使用者輸入的密碼進行遮罩顯示
 * @param {string} password 需遮罩密碼
 * @returns {string}
 */
function passwordMask(password) {
    let maskPwd = '';
    for(let i=0 ; i<password.length ; i++){
        if(i === 0 || i === password.length-1){
            maskPwd = maskPwd + password[i];
        }else{
            maskPwd = maskPwd + '*';
        }
    }
    return maskPwd;
}

/**
 * 格式化日期格式為yyyy-MM-dd
 * @param {object} date 要處理的date物件
 * @returns {string}
 */
function formatDate(date) {
    let year = date.getFullYear().toString();
    let month = (date.getMonth() + 1).toString();
    let day = date.getDate().toString();

    if(month.length < 2){
        month = "0" + month;
    }
    if(day.length < 2){
        day = "0" + day;
    }
    return  year + '-' + month + '-' + day;
}

/**
 * 將浮點數四捨五入至指定位數
 * @param val 要處理的數值
 * @param {int} precision 小數點後四捨五入的位數
 * @returns {string}
 */
function roundDecimal(val, precision) {
    if(parseFloat(val)){
        if(val < 0){
            val = 0 - val;
            return (0 - (Math.round(Math.round(val * Math.pow(10, (precision || 0) + 1)) / 10) / Math.pow(10, (precision || 0)))).toString();
        }else{
            return (Math.round(Math.round(val * Math.pow(10, (precision || 0) + 1)) / 10) / Math.pow(10, (precision || 0))).toString();
        }
    }else{
        return val.toString();
    }
}

/**
 * 數字加上千分位分隔符號及幣別符號
 * @param n 要加工的數值
 * @param {string} currency_sign 要加上的幣別符號
 * @param {int} precision 顯示的小數位數
 * @returns {string}
 */
function FormatNumber(n, currency_sign = '', precision = 0) {
    n = roundDecimal(n, precision);
    let arr = n.split(".");

    // format小數點位數顯示
    if(arr[1] !== undefined){
        if(arr[1] === '00') {  // 若小數均為0則去掉小數不顯示
            arr.splice(1, 1);
        }
        else if(arr[1].substring(1, 2) === '0' && arr[1].length < 3){  // 若小數第二位為0且沒有第三位數則去掉不顯示
            arr[1] = arr[1].substring(0, 1);
        }else{
            if(arr[1].substring(2, 3) === '0'){  // 若小數第三位為0則去掉不顯示
                arr[1] = arr[1].substring(0, 2);
            }
        }
    }

    let re = /(\d{1,3})(?=(\d{3})+$)/g;
    return currency_sign + arr[0].replace(re, "$1,") + (arr.length == 2 ? "." + arr[1] : "");
}

/**
 * 將數字加上顏色
 * @param {string} n 要加工的數值
 * @param class_name 要加上的class name
 */
function addNumberColor(n, class_name = 'text-danger') {
    if(n.indexOf('-') !== -1 || n.indexOf('+') !== -1){
        return '<span class="' + class_name + '">' + n + '</span>';
    }else{
        return n;
    }
}

/**
 * 計算兩個日期之間的天數差
 * @param sDate1
 * @param sDate2
 * @return {number}
 */
function DateDiff(sDate1, sDate2) {
    let aDate, oDate1, oDate2, iDays;
    aDate = sDate1.split("-");
    oDate1 = new Date(aDate[0] + '-' + aDate[1] + '-' + aDate[2]);
    aDate = sDate2.split("-");
    oDate2 = new Date(aDate[0] + '-' + aDate[1] + '-' + aDate[2]);

    iDays = parseInt(Math.abs(oDate1 - oDate2) / 1000 / 60 / 60 / 24); // 把相差的毫秒數轉換為天數

    return iDays;
}

/**
 * 取得廳別對應的幣別符號
 * @param root_hall 對應的根廳別
 * @param hall_name 對應的廳別
 * @return {string} 幣別符號
 */
function getHallCurrencySign(root_hall, hall_name) {
    return hall_config_dict[root_hall][hall_name].currency_sign;
}

/**
 * 取得廳別對應的幣別符號文字
 * @param root_hall 對應的根廳別
 * @param hall_name 對應的廳別
 * @return {string} 幣別符號文字
 */
function getCurrencySignText(root_hall, hall_name) {
    return '(單位:' + hall_config_dict[root_hall][hall_name].currency_sign_name + hall_config_dict[root_hall][hall_name].currency_sign + ')';
}

/**
 * ajax發送後，將表單及modal確認按鈕disabled，並顯示Loading視窗
 * @param {Object[]} disableBtnAry 需要disabled的按鈕
 * @param {string} hide_modal_id 需要隱藏的modal id
 */
function startSendingDataWithModal(disableBtnAry, hide_modal_id= 'modal'){
    $('#' + hide_modal_id).modal('hide');  // 將確認視窗modal關閉

    for(let i=0 ; i<disableBtnAry.length ; i++){
        disableBtnAry[i].prop('disabled', true);
    }

    $('#modal_body_text_selected_name').text('');
    showLoadingModal();  // 顯示Loading視窗
}

/**
 * ajax發送完成後，將表單及modal確認按鈕enabled，並關閉Loading視窗
 * @param {Object[]} enabledBtnAry 需要enabled的按鈕
 * @param {boolean} is_need_close_loading 是否需要關閉loading視窗
 * @param {String} target_modal
 */
function endSendingDataWithModal(enabledBtnAry, is_need_close_loading, target_modal = ''){
    $('#clickConfirmBtn').val('false');  // 將clickConfirmBtn設為false，避免表單直接submit

    for(let i=0 ; i<enabledBtnAry.length ; i++){
        enabledBtnAry[i].prop('disabled', false);
    }

    if(is_need_close_loading){
        closeLoadingModal(target_modal);  // 關閉Loading視窗
    }
}

/**
 * 返回value值对应的key
 * @param {object} obj 要尋找的字典物件
 * @param {string} value 尋找的值
 * @param compare
 * @returns {string}
 */
function findKey (obj,value, compare = (a, b) => a === b) {
    return Object.keys(obj).find(k => compare(obj[k], value))
}

/**
 * 返回tag_value值对应的tag_key
 * @param {object} tag_dict 要尋找的標籤字典
 * @param {string} tag_value 尋找的標籤名稱
 * @param compare
 * @returns {string}
 */
function findTagKey (tag_dict,tag_value, compare = (a, b) => a === b) {
    return Object.keys(tag_dict).find(k => compare(tag_dict[k].tag_value, tag_value))
}

/**
 * 返回date_entity值对应的key陣列(找出對應年月週的日期陣列)
 * @param {object} date_entity 尋找的日期物件
 * @returns {[String]}
 */
function findDateWeekMappingKeyAry (date_entity) {
    let date_ary = [];
    let find_mapping_date = function (date_key) {
        if(date_week_mapping_dict[date_key].fin_year === date_entity.fin_year && date_week_mapping_dict[date_key].fin_month === date_entity.fin_month && date_week_mapping_dict[date_key].fin_week === date_entity.fin_week){
            date_ary.push(date_key);
        }
    };
    Object.keys(date_week_mapping_dict).find(find_mapping_date);

    return date_ary;
}

/**
 * 返回value值对应的key(找出對應的廳別名稱)
 * @param {string[]} root_hall_ary 要尋找的根廳別
 * @param {object} value 尋找的廳別物件
 * @param compare
 */
function findHallIdMappingKey (root_hall_ary, value, compare = (a, b) => a.hall_id === b.hall_id && a.domain_id === b.domain_id) {
    let key = undefined;
    let index = 0;
    while(key === undefined && index<root_hall_ary.length){
        key = Object.keys(hall_config_dict[root_hall_ary[index]]).find(k => compare(hall_config_dict[root_hall_ary[index]][k], value));
        index++;
    }
    return key;
}

/**
 * 返回輸入廳別的根廳別
 * @param hall_name 要搜尋的廳別
 */
function findRootHall(hall_name) {
    let root_hall = null;
    $.each(hall_config_dict, function(root_key,root_value) {
        $.each(hall_config_dict[root_key], function(key,value) {
            if(key == hall_name){
                root_hall = root_key;
                return false;
            }
        });
        if(root_hall !== null){
            return false;
        }
    });
    return root_hall;
}

/**
 * 計算傳入的日期區間橫跨幾個帳務週
 * @param {string} date_span 要計算的日期區間
 * @return {int}
 */
function calculate_financial_week_span(date_span) {
    let date_ary = date_span.split('~');
    let start_date = new Date(date_ary[0]);
    let end_date = new Date(date_ary[1]);
    let tmp_date = end_date;
    let day_span = parseInt((end_date - start_date) / 1000 / 60 / 60 / 24);  // 取得橫跨天數
    let week_span = 1;  // 紀錄橫跨的週期
    while(day_span > 0){
        let current_date_entity = date_week_mapping_dict[formatDate(tmp_date)];
        tmp_date.setDate(tmp_date.getDate() - 1);
        day_span--;
        let pre_date_entity = date_week_mapping_dict[formatDate(tmp_date)];

        // 判斷current_date和pre_date是否為同一帳務週
        if(!(current_date_entity.fin_year === pre_date_entity.fin_year &&
            current_date_entity.fin_month === pre_date_entity.fin_month &&
            current_date_entity.fin_week === pre_date_entity.fin_week)){
            week_span++;
        }
    }
    return week_span;
}

/**
 * 設定返回按鈕
 */
function set_go_back_btn() {
    $('.go_back_btn').removeAttr('onclick');  // 將頁面新增的onclick事件移除
    $('.go_back_btn').unbind('click');  // 將jquery綁定的onclick事件移除

    if(window.pageRecordStack.length === 0){
        $('.go_back_btn').prop('disabled', true);
    }else{
        let last_page_index = window.pageRecordStack.length - 1;
        let go_back_folder = window.pageRecordStack[last_page_index].go_back_folder;
        let go_back_page = window.pageRecordStack[last_page_index].go_back_page;
        let is_need_show_loading = window.pageRecordStack[last_page_index].is_need_show_loading;

        if(go_back_folder !== undefined && go_back_page !== undefined){
            //  替返回按鈕綁定onclick事件
            $('.go_back_btn').on('click', function () {
                window.pageRecordStack.pop();  // 將返回的頁面從stack移除

                highLightSidebarItemByID(go_back_page);

                if(window.pageRecordStack.length !== 0){
                    redirectToPage(go_back_folder, go_back_page, is_need_show_loading, false);
                }else{
                    redirectToPage(go_back_folder, go_back_page, is_need_show_loading);
                }
            });
            $('.go_back_btn').prop('disabled', false);
        }
    }
}

/**
 * 顯示詳細資料modal視窗
 * @param pageName 轉導頁面名稱
 * @param content_config
 * @param modal_config
 * @param callback
 */
function showDetailModal(pageName,
                         content_config =
                             {
                                 template_element: undefined,
                                 folder_path: undefined,
                             },
                         modal_config =
                             {
                                 extra_class: {
                                     modal_dialog: '',
                                     modal_content: '',
                                     modal_header: '',
                                     modal_body: '',
                                 },
                                 header_content: {
                                     header_title: '',
                                     header_name: ''
                                 },
                                 close_target: ''
                             },
                         callback = function () {}
                         ) {
    let modal_id = pageName + '_modal';

    let modal_html =
        '<div class="modal fade" id="' + modal_id + '" data-backdrop="static">' +
            '<div class="modal-dialog modal-dialog-scrollable ' + modal_config.extra_class.modal_dialog + '">' +
                '<div class="modal-content ' + modal_config.extra_class.modal_content + '">' +
                    '<div class="modal-header ' + modal_config.extra_class.modal_header + '">' +
                        '<div class="modal_header_loading" style="display: none;">' +
                            '<img src="./dist/img/loading.gif" class="img-size-32">' +
                        '</div>' +
                        '<div class="cdp__modal__detail-header-div modal_header_div">' +
                            '<label class="modal_header_title cdp__modal__detail-header-title mr-2">' +
                                modal_config.header_content.header_title +
                            '</label>' +
                            '<label class="modal_header_name cdp__modal__detail-header-name">' +
                                modal_config.header_content.header_name +
                            '</label>' +
                        '</div>' +
                        '<button type="button" class="close cdp__modal__detail-header-close" data-dismiss="modal" data-toggle="modal" data-target="#' + modal_config.close_target + '_modal" aria-label="Close">' +
                            '<span aria-hidden="true">&times;</span>' +
                        '</button>' +
                    '</div>' +
                    '<div class="modal-body ' + modal_config.extra_class.modal_body + '">' +
                    '</div>' +
                '</div>' +
            '</div>' +
        '</div>';

    $("#content").append(modal_html);

    // 綁定modal事件
    $('#'+ modal_id).on('hidden.bs.modal', function () {
        //  若頁面存在顯示的modal，則加上class，避免scrollbar跑版
        if($('body .modal.show').length !== 0){
            $('body').addClass('modal-open');
        }

        if(!($(this).hasClass('temporary_hide'))){
            // 將modal移除
            $('#'+ modal_id).remove();

            destroyChartJS_modal_popup(pageName);  // 清除detail modal chart.js物件
            destroyHighChart_modal_popup(pageName);  // 清除detail modal HighChart物件
        }
    });

    $('#'+ modal_id).on('show.bs.modal', function () {
        if(($(this).hasClass('temporary_hide'))){
            $(this).removeClass('temporary_hide');
        }
    });

    //  將目前已開啟的modal視窗加上暫存class標記並先將其關閉
    close_displayed_modal(function () {
        // 載入指定的page
        if(content_config.folder_path){
            let path = 'pages/' + content_config.folder_path  + pageName + '.html';
            $('#'+ modal_id + ' .modal-body').load(path + '?' + Date.now(), function(){
                $('#'+ modal_id).modal('show');  // 顯示明細彈出視窗
            });
        }else if(content_config.template_element){
            $('#'+ modal_id + ' .modal-body').html(content_config.template_element.html());
            callback();
            $('#'+ modal_id).modal('show');  // 顯示明細彈出視窗
        }
    });
}

/**
 * 將目前已顯示的modal視窗暫時隱藏
 * @param callback
 */
function close_displayed_modal(callback) {
    let displayed_modal = $('body .modal.show');

    displayed_modal.addClass('temporary_hide');
    displayed_modal.modal('hide');
    callback();
}

/**
 * 新增/修改modal視窗(增加確認與詢問是否放棄修改視窗，且class移除fade動畫效果)
 * @param pageName
 * @param content_config
 * @param modal_config
 * @param callback
 */
function addEditModal(pageName,
                      content_config =
                          {
                              template_element: undefined,
                              folder_path: undefined,
                          },
                      modal_config =
                          {
                              extra_class: {
                                  modal_dialog: '',
                                  modal_content: '',
                                  modal_header: '',
                                  modal_body: '',
                              },
                              header_content: {
                                  header_title: '',
                                  header_name: ''
                              }
                          },
                      callback = function () {}
                      ) {
    let modal_id = pageName + '_modal';

    let modal_html =
        '<div class="modal" id="' + modal_id + '" data-backdrop="static">' +
        '<div class="modal-dialog modal-dialog-scrollable ' + modal_config.extra_class.modal_dialog + '">' +
        '<div class="modal-content ' + modal_config.extra_class.modal_content + '">' +
        '<div class="modal-header ' + modal_config.extra_class.modal_header + '">' +
        '<div class="modal_header_loading" style="display: none;">' +
        '<img src="./dist/img/loading.gif" class="img-size-32">' +
        '</div>' +
        '<div class="cdp__modal__detail-header-div modal_header_div">' +
        '<label class="modal_header_title cdp__modal__detail-header-title mr-2">' +
        modal_config.header_content.header_title +
        '</label>' +
        '<label class="modal_header_name cdp__modal__detail-header-name">' +
        modal_config.header_content.header_name +
        '</label>' +
        '</div>' +
        '<button type="button" class="close cdp__modal__detail-header-close" data-dismiss="modal" data-toggle="modal" data-target="#' + modal_id + '_alert_cancel' + '" aria-label="Close">' +
        '<span aria-hidden="true">&times;</span>' +
        '</button>' +
        '</div>' +
        '<div class="modal-body ' + modal_config.extra_class.modal_body + '">' +
        '</div>' +
        '</div>' +
        '</div>' +
        '</div>';

    addCancelModal(modal_id);
    $("#content").append(modal_html);
    addConfirmModal(modal_id);

    // 載入指定的page
    if(content_config.folder_path){
        let path = 'pages/' + content_config.folder_path  + pageName + '.html';
        $('#'+ modal_id + ' .modal-body').load(path + '?' + Date.now(), function(){});
    }else if(content_config.template_element){
        $('#'+ modal_id + ' .modal-body').html(content_config.template_element.html());
    }

    callback();
}

/**
 * 是否放棄修改modal視窗
 * @param modal_id
 */
function addCancelModal(modal_id) {
    let modal_html = '<div class="modal" id="' + modal_id + '_alert_cancel" data-backdrop="static">' +
        '<div class="modal-dialog modal-dialog-centered cdp__modal-dialog cdp-w__300">' +
        '<div class="modal-content cdp__modal-alert">' +
        '<div class="cdp__modal-alert-header-img float-left">' +
        '<img src="./dist/img/alert-2.png" alt="update-icon" />' +
        '</div>' +
        '<div class="cdp__modal-alert-body mb__38">' +
        '<span class="cdp__modal-alert-body-title cdp__text__light-blue mb__20">' +
        '尚未儲存' +
        '</span>' +
        '<span class="cdp__modal-alert-body-desc cdp__text-onyx">' +
        '是否要儲存異動內容？' +
        '</span>' +
        '</div>' +
        '<div class="cdp__modal-alert-footer mb__10">' +
        '<button id="' + modal_id + '_alert_cancel_cancel_btn" class="cdp__modal-btn__cancel ml__10 mr__5 b__radius-8" data-dismiss="modal">' +
        '<span class="cdp__modal-btn__cancel-text">' +
        '不儲存' +
        '</span>' +
        '</button>' +
        '<button id="' + modal_id + '_alert_cancel_confirm_btn" class="cdp__modal-btn__submit mr__10 ml__5 custom-bg-dark__blue b__radius-8" data-dismiss="modal" data-toggle="modal" data-target="#' + modal_id + '" data-btn_from="alert_cancel">' +
        '<span class="cdp__modal-btn__submit-text">' +
        '儲存' +
        '</span>' +
        '</button>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '</div>';

    $("#content").append(modal_html);
}

/**
 * 資料新增/修改確認modal視窗
 * @param modal_id
 * @param modal_config
 * @param callback
 */
function addConfirmModal(modal_id,
                         modal_config =
                             {
                                 header_content: '請確認資料是否正確',
                                 cancel_btn_text: '修改'
                             },
                         callback = function () {}
                         ) {
    let modal_html =  '<div class="modal" id="' + modal_id + '_alert_confirm" data-backdrop="static">' +
        '<div class="modal-dialog modal-dialog-centered cdp__modal-dialog cdp-w__300">' +
        '<div class="modal-content cdp__modal-alert">' +
        '<div class="cdp__modal-alert-header-img float-left">' +
        '<img src="./dist/img/alert-2.png" alt="update-icon" />\n' +
        '</div>' +
        '<div class="cdp__modal-alert-body mb__38">' +
        '<span class="cdp__modal-alert-body-title cdp__text__light-blue mb__20" id="' + modal_id + '_alert_confirm_body_title">' +
        modal_config.header_content +
        '</span>' +
        '<div class="cdp__modal-alert-body-desc cdp__text-onyx px__10" id="' + modal_id + '_alert_confirm_body_desc">' +
        '</div>' +
        '</div>' +
        '<div class="cdp__modal-alert-footer mb__10">' +
        '<button id="' + modal_id + '_alert_confirm_cancel_btn" class="cdp__modal-btn__cancel ml__10 mr__5 b__radius-8" data-dismiss="modal" data-toggle="modal" data-target="#' + modal_id + '">' +
        '<span class="cdp__modal-btn__cancel-text">' +
        modal_config.cancel_btn_text +
        '</span>' +
        '</button>' +
        '<button id="' + modal_id + '_alert_confirm_confirm_btn" class="cdp__modal-btn__submit mr__10 ml__5 custom-bg-dark__blue b__radius-8" data-dismiss="modal">' +
        '<span class="cdp__modal-btn__submit-text">' +
        '確定' +
        '</span>' +
        '</button>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '</div>';

    $("#content").append(modal_html);

    callback();
}

/**
 * 關閉detail modal視窗
 */
function closeDetailModal(pageName) {
    setTimeout(function(){  // 等待0.1秒後才關閉loading視窗
        $('#' + pageName + '_modal').modal('hide');  // 關閉loading視窗
    }, 100);
}

/**
 * 新增刪除modal視窗
 * @param pageName
 * @param callback 要綁定的事件
 */
function addDeleteModal(pageName, callback) {
    let modal_id = pageName + '_modal';
    let modal_html = '<div class="modal" id="' + modal_id + '_alert_delete" data-backdrop="static">' +
        '<div class="modal-dialog modal-dialog-centered cdp__modal-dialog cdp-w__300">' +
        '<div class="modal-content cdp__modal-alert">' +
        '<div class="cdp__modal-alert-header-img float-left">' +
        '<img src="./dist/img/alert-1.png" alt="delete-icon" />' +
        '</div>' +
        '<div class="cdp__modal-alert-body mb__38">' +
        '<span class="cdp__modal-alert-body-title cdp__text__red mb__20">' +
        '刪除' +
        '</span>' +
        '<span class="cdp__modal-alert-body-desc cdp__text-onyx">' +
        '確認刪除「<span id="' + modal_id + '_alert_delete_content"></span>」？' +
        '</span>' +
        '</div>' +
        '<div class="cdp__modal-alert-footer mb__10">' +
        '<button class="cdp__modal-btn__cancel ml__10 mr__5 b__radius-8" data-dismiss="modal">' +
        '<span class="cdp__modal-btn__cancel-text">' +
        '取消' +
        '</span>' +
        '</button>' +
        '<button id="' + modal_id + '_alert_delete_confirm_btn" class="cdp__modal-btn__delete mr__10 ml__5 custom-bg-red b__radius-8">' +
        '<span class="cdp__modal-btn__delete-text">' +
        '確定' +
        '</span>' +
        '</button>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '</div>';

    $("#content").append(modal_html);

    callback();
}

/**
 * 顯示上傳名單彈出視窗
 */
function showUploadModal(upload_config, callback = function () {}, switch_id = '', import_config = undefined, is_need_close_loading = false) {
    let modal_html = '<div class="modal" id="modal_upload" data-backdrop="static">' +
        '</div>';
    $('#content').append(modal_html);
    
    $('#modal_upload').load('pages/modal_template/common/upload_file.html' + '?' + Date.now(), function () {
        // event binding
        $('#upload_fin_confirm_btn').on('click', function () {
            if ($('#upload_form').valid()) {
                upload_and_import_file(upload_config, import_config, is_need_close_loading,
                    function () {
                    if(switch_id){
                        $('.use_custom_list_switch').prop('checked', true);  // 將同一頁面所有上傳名單按鈕開啟
                    }
                    callback();  // 發查列表
                }, function () {
                    if(switch_id){
                        $('#' + switch_id).prop('checked', false);
                    }
                });
            }
        });

        $('.upload_file_cancel_btn').on('click', function () {
            if(switch_id){
                $('#' + switch_id).prop('checked', false);
            }

            $('#modal_upload').on('hidden.bs.modal', function () {
                $('#modal_upload').remove();
            });
        });

        $('#modal_upload').modal('show');
    });
}

/**
 * 當modal顯示時，header和footer會小跑版，調整header和footer寬度
 * @param {int} windowBodyWidth 要調整的寬度(px)
 */
function adjustFixedMargin(windowBodyWidth){
    let scrollbarWidth = document.body.clientWidth - windowBodyWidth;  // 取得scrollbar寬度

    if(scrollbarWidth !== 0){
        window.scrollbarWidth = scrollbarWidth;  // 紀錄scrollbar寬度
    }

    $('#include_header').css('margin-right', window.scrollbarWidth + 'px');
    $('#include_footer').css('margin-right', window.scrollbarWidth + 'px');
}

/**
 * modal關閉時，恢復header和footer寬度
 */
function removeFixedMargin(){
    $('#include_header').css('margin-right', '');
    $('#include_footer').css('margin-right', '');
}

/**
 * 顯示Loading modal視窗
 */
function showLoadingModal(){
    $('#modal-loading').modal('show');  // 顯示loading視窗
}

/**
 * 關閉Loading modal視窗
 */
function closeLoadingModal(target_modal = '') {
    let reSetTarget = function(callback){
        $('#modal-loading_close_btn').attr('data-target', '#' + target_modal);
        callback();
    };

    reSetTarget(function () {
        $('#modal-loading_close_btn').click();  // 關閉loading視窗
    });
}

/**
 * 顯示Loading modal視窗(modal & popup)
 */
function showLoadingModal_popup(page_name){
    if(getCurrentPageName(location.pathname) === 'popup_index.html'){
        $('#popup_header_loading').show();  // 顯示loading視窗
        $('#popup_header_div').hide();
    }else{
        $('#' + page_name + '_modal .modal_header_loading').show();  // 顯示loading視窗
        $('#' + page_name + '_modal .modal_header_div').hide();
    }
}

/**
 * 關閉Loading modal視窗(modal & popup)
 */
function closeLoadingModal_popup(page_name) {
    setTimeout(function(){  // 等待0.1秒後才關閉loading視窗
        if(getCurrentPageName(location.pathname) === 'popup_index.html'){
            $('#popup_header_loading').hide();  // 關閉loading視窗
            $('#popup_header_div').show();
        }else{
            $('#' + page_name + '_modal .modal_header_loading').hide();  // 關閉loading視窗
            $('#' + page_name + '_modal .modal_header_div').show();
        }
    }, 100);
}

/**
 * modal彈出視窗內容(中等間距)
 * @param {object} contentAry 彈出視窗要顯示的內容
 * @returns {string}
 */
function modalConfirmContent_md(contentAry){
    let rtnStr = '<table style="width: 100%">';
    $.each(contentAry, function (key, value) {
        rtnStr = rtnStr + '<tr>' +
            '<td class="text-nowrap text-right" style="width: 20%; vertical-align:text-top; word-break:break-all;">' + key  + '</td>' +
            '<td class="text-nowrap text-center" style="width: 2%; vertical-align:text-top;">：</td>' +
            '<td class="text-left" style="width: 78%; vertical-align:text-top; word-break:break-all;">' + value + '</td>' +
            '</tr>';
    });
    rtnStr = rtnStr + '</table>';
    return rtnStr;
}

/**
 * modal彈出視窗內容(小間距)
 * @param {object} contentAry 彈出視窗要顯示的內容
 * @returns {string}
 */
function modalConfirmContent_sm(contentAry){
    let rtnStr = '<table style="width: 100%">';
    $.each(contentAry, function (key, value) {
        rtnStr = rtnStr + '<tr>' +
            '<td class="text-nowrap text-right" style="width: 10%; vertical-align:text-top; word-break:break-all;">' + key  + '</td>' +
            '<td class="text-nowrap text-center" style="width: 2%; vertical-align:text-top;">：</td>' +
            '<td class="text-left" style="width: 88%; vertical-align:text-top; word-break:break-all;">' + value + '</td>' +
            '</tr>';
    });
    rtnStr = rtnStr + '</table>';
    return rtnStr;
}

/**
 * 另開詳細資料視窗
 * @param folder_path 目錄路徑
 * @param pageName 轉導頁面名稱
 */
function openDetailWindow(folder_path, pageName,) {
    let open_detail_win_entity = {
        from_page : window.fromPage,
        open_detail_win_folder : folder_path,
        open_detail_win_pageName : pageName
    };
    sessionStorage.setItem('open_detail_win', JSON.stringify(open_detail_win_entity));
    window.open('popup_index.html?' + Date.now(), 'CDP pop_window_' + new Date().getTime(), config='height=800,width=570')
}

/**
 * 產生帳務週次下拉選單
 * @param element_id 下拉選單元件id
 * @param year 帳務年
 * @param month 帳務月
  */
function generate_week_dropdown_list(element_id, year, month) {
    let dropdownList = document.getElementById(element_id);
    $(dropdownList).find('option').remove();  //  將全部的option移除

    let cur_week = '1';
    let start_date = '';
    let end_date = '';
    let date_range = '';
    $.each(date_week_mapping_dict, function(key, value) {
        if(value.fin_year === year && value.fin_month === month){
            //  先找出該年月的第一週起始日
            if(start_date === ''){
                start_date = key;
            }
            if(cur_week !== value.fin_week){
                date_range = ' (' + start_date + ' ~ ' + end_date + ')';

                let week_option = new Option(cur_week + date_range, cur_week, false, false);
                $(dropdownList).append(week_option);

                cur_week = value.fin_week;
                start_date = key
            }
            end_date = key;
        }
    });

    //  將最後一週的資料補上
    date_range = ' (' + start_date + ' ~ ' + end_date + ')';
    let week_option = new Option(cur_week + date_range, cur_week, false, false);
    $(dropdownList).append(week_option);
}

/**
 * 設定查詢廳別下拉選單選項(可依不同使用者權限將選項disabled或移除)
 * @param {string} element_id 下拉選單id
 * @param {string[]} root_hall_name_ary 要產生的根廳別名稱
 * @param {boolean} disabled_by_user 選項是否依據使用者權限disabled
 * @param {object} disabled_hall_dict 不產生於下拉選單的廳別
 */
function setHallDropdownList(element_id, root_hall_name_ary, disabled_by_user, disabled_hall_dict = {}) {
    let dropdownList = document.getElementById(element_id);

    //  初始化下拉選單選項
    for(let i=0 ; i<root_hall_name_ary.length ; i++) {
        $.each(hall_config_dict[root_hall_name_ary[i]], function(key,value) {
            if(disabled_hall_dict.hasOwnProperty(key)){  //  若要新增的廳別有在禁用廳別中，則跳過不新增
                return;  // 相當於continue
            }
            let hall_option = new Option(value.hall_name, key, false, false);
            $(dropdownList).append(hall_option);
        });
    }

    //  設定選項權限
    if(disabled_by_user){
        let option_ary = $(dropdownList).find('option');
        let access_hall_ary = getSessionStorageEntity('user_info').access_hall.split(',');
        let index = 0;
        for(let i=index ; i<option_ary.length ; i++){
            let can_access = false;
            for(let j=0 ; j<access_hall_ary.length ; j++){
                if(option_ary[i].value === access_hall_ary[j]){
                    can_access = true;
                    break;
                }
            }
            if(!can_access){
                // $(option_ary[i]).append(' (無權限)');
                // $(option_ary[i]).prop('disabled', 'disabled');
                $(option_ary[i]).remove();
            }
        }
    }
    $(dropdownList).val('');  // 預設不選中廳別
}

/**
 * 設定動態取資料的下拉選單選項
 * @param element_id 下拉選單id
 * @param key_array option value
 * @param value_array option text
 * @param generate_all_option
 * @param dropdown_parent
 */
function setDynamicDropdownList(element_id, key_array, value_array, generate_all_option = false, dropdown_parent = '') {
    let dropdownList = document.getElementById(element_id);
    $(dropdownList).find('option').remove();  // 清空option

    //  初始化下拉選單選項
    if(generate_all_option){
        let option = new Option('全部', '', false, false);
        $(dropdownList).append(option);
    }

    for(let i=0 ; i<key_array.length ; i++) {
        let option = new Option(value_array[i], key_array[i], false, false);
        $(dropdownList).append(option);
    }
    $(dropdownList).select2({
        dropdownCssClass: 'cdp__dropdown__content',
        dropdownParent: dropdown_parent ? $('#' + dropdown_parent) : $(document.body)
    });  // 重新初始化select2
}

/**
 * chart.js隨機產生背景顏色
 * @param {number} alpha 顏色透明度
 * @returns {string} 顏色rgb字串
 */
function dynamicBackgroundColors(alpha){
    let r = Math.floor(Math.random() * 255);
    let g = Math.floor(Math.random() * 255);
    let b = Math.floor(Math.random() * 255);

    return "rgb(" + r + "," + g + "," + b + "," + alpha + ")";
}

/**
 * chart.js隨機產生背景顏色
 * @param {int[]} rgb_ary 顏色rgb數值
 * @param {number} alpha 顏色透明度
 * @returns {string} 顏色rgb字串
 */
function generateRGBColors(rgb_ary, alpha){
    return "rgb(" + rgb_ary[0] + "," + rgb_ary[1] + "," + rgb_ary[2] + "," + alpha + ")";
}

/**
 * 顯示圖表訊息(載入中or錯誤訊息)
 * @param {string} element_id 要隱藏的element id
 * @param {string} msg 要顯示的文字
 * @param {string} type 要顯示的圖案種類
 */
function showMsgDiv(element_id, msg, type) {
    // 表格經過datatable套件初始化後dom結構會改變，須先判斷是否已經經過datatable初始化
    if($('#' + element_id + '_wrapper').length > 0){
        element_id = element_id + '_wrapper';
    }

    $('#' + element_id).hide();
    if(type === 'loading'){
        $('#' + element_id).parent().find('.no_result_msg').html('<i class="fas fa-spinner fa-spin mr-1"></i>' + msg).show();
    }else if(type === 'info'){
        $('#' + element_id).parent().find('.no_result_msg').html('<i class="fas fa-info-circle mr-1"></i>' + msg).show();
    }else{
        $('#' + element_id).parent().find('.no_result_msg').html('<i class="fas fa-exclamation-triangle mr-1"></i>' + msg).show();
    }
}

/**
 * 隱藏圖表訊息
 * @param {string} element_id 要顯示的element id
 */
function hideMsgDiv(element_id) {
    // 表格經過datatable套件初始化後dom結構會改變，須先判斷是否已經經過datatable初始化
    if($('#' + element_id + '_wrapper').length > 0){
        element_id = element_id + '_wrapper';
    }

    $('#' + element_id).parent().find('.no_result_msg').hide();
    $('#' + element_id).show();
}

/**
 * 產生成長或下降百分比數值
 * @param {string} percent 要加工的數值
 * @param show_color 是否套用顏色
 * @returns {string}
 */
function showComparePercent(percent, show_color = true) {
    return percent.indexOf('-') !== -1 ?
        percent === '-0' ?
            show_color ?
                '<span class="text-secondary"><i class="fas fa-caret-left mr-1"></i>' + FormatNumber(percent.replace('-', '')) + '%</span>' :
                '<i class="fas fa-caret-left mr-1"></i>' + FormatNumber(percent.replace('-', '')) + '%' :
            show_color ?
                '<span class="text-danger"><i class="fas fa-caret-down mr-1"></i>' + FormatNumber(percent.replace('-', '')) + '%</span>' :
                '<i class="fas fa-caret-down mr-1"></i>' + FormatNumber(percent.replace('-', '')) + '%' :
        percent === '0' ?
            show_color ?
                '<span class="text-secondary"><i class="fas fa-caret-left mr-1"></i>' + FormatNumber(percent) + '%</span>' :
                '<i class="fas fa-caret-left mr-1"></i>' + FormatNumber(percent) + '%' :
            show_color ?
                '<span class="text-success"><i class="fas fa-caret-up mr-1"></i>' + FormatNumber(percent) + '%</span>' :
                '<i class="fas fa-caret-up mr-1"></i>' + FormatNumber(percent) + '%';
}

/**
 * 產生名次上升或下降數值
 * @param rank_growth 要加工的數值
 * @return {string}
 */
function showCompareRank(rank_growth) {
    return rank_growth.length === 1 ?
        rank_growth.indexOf('-') !== -1 ?
            '<span class="text-md">NEW</span>' :
            rank_growth === '0' ?
                '' :
                '<span class="text-md"><i class="fas fa-angle-double-up mr-1"></i>' + FormatNumber(rank_growth) + '</span>' :
        rank_growth.indexOf('-') !== -1 ?
            '<span class="text-md"><i class="fas fa-angle-double-down mr-1"></i>' + FormatNumber(rank_growth.replace('-', '')) + '</span>' :
            '<span class="text-md"><i class="fas fa-angle-double-up mr-1"></i>' + FormatNumber(rank_growth) + '</span>';
}

/**
 * 設定查詢廳別checkbox選項
 * @param source_page
 * @param {string} element_id 要設定的checkbox div id
 */
function setHallCheckBox(source_page, element_id) {
    let checkBoxDiv = document.getElementById(element_id);

    //  初始化checkbox選項
    let checkbox_index = 1;
    $.each(hall_config_dict, function(key,value) {
        let checkbox_html = '<div class="col-md-6">' +
            '<p class="mb-1 font-italic font-weight-bold">' + key + '</p>';
        $.each(hall_config_dict[key], function(key,value) {
            checkbox_html = checkbox_html +
                '<div class="icheck-primary d-inline mr-3">' +
                '<input class="class_access_hall" type="checkbox" ' +
                'id="' + source_page + '_checkbox_' + checkbox_index + '" name="hall_checkbox" value="' + key + '">' +
                '<label class="font-weight-normal text-black" style="opacity: 100%" ' +
                'for="' + source_page + '_checkbox_' + checkbox_index + '">' +
                '<span class="ml-n2">【' + key + '】' + value.hall_name + '</span>' +
                '</label>' +
                '</div>';
            checkbox_index++;
        });
        checkbox_html = checkbox_html + '</div>';
        $(checkBoxDiv).append(checkbox_html);
    });

    //  若頁面上有目標族群section則將checkbox綁定事件
    if($('#' + source_page + '_custom_tags_div') !== undefined){
        let checkbox_el_ary = $('#' + element_id).find('.icheck-primary label');
        for(let i=0 ; i<checkbox_el_ary.length ; i++){
            $(checkbox_el_ary[i]).on('click', function () {
                if($(checkbox_el_ary[i]).parent().find('input').is(':checked')){
                    setCustomTagsHallSelectOption(source_page, this);
                }else{
                    setCustomTagsHallSelectOption(source_page, this);
                }
            })
        }
    }
}

/**
 * 檢查標籤的可用狀況
 * @param hall_name 檢查的廳別
 * @param tag_code 檢查的標籤代碼
 */
function checkTagUsage(hall_name, tag_code) {
    let tag_description_dict = getSessionStorageEntity('system_config').tags_config[hall_name];
    return tag_description_dict[tag_code] && tag_description_dict[tag_code].tag_enabled;
}

/**
 * 產生標籤的badge圖案
 * @param hall_name 要產生的標籤廳別
 * @param tag_code 要產生的標籤代碼
 * @param badge_text_class 要產生的badge文字大小class
 * @return {string}
 */
function generateTagsBadge(hall_name, tag_code, badge_text_class = '') {
    let rtn_html = '';
    let tag_description_dict = getSessionStorageEntity('system_config').tags_config[hall_name];
    if(tag_description_dict[tag_code] !== undefined){
        let tag_type = tag_description_dict[tag_code].tag_type;

        rtn_html += '<li class="list-inline-item">' +
            '<a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title data-original-title="' + tag_description_dict[tag_code].tag_description + '">';

        if(tag_type === 1){
            rtn_html += '<span class="badge badge-custom-danger ' + badge_text_class + '">';
        }else if(tag_type === 2){
            rtn_html += '<span class="badge badge-custom-green ' + badge_text_class + '">';
        }else if(tag_type === 3){
            rtn_html += '<span class="badge badge-custom-blue ' + badge_text_class + '">';
        }else if(tag_type === 4){
            rtn_html += '<span class="badge badge-custom-yellow ' + badge_text_class + '">';
        }else if(tag_type === 5){
            rtn_html += '<span class="badge badge-custom-danger ' + badge_text_class + '">';
        }

        rtn_html += tag_description_dict[tag_code].tag_name + '</span>' +
            '<span style="display: none;">' + tag_code +'</span>' +
            '</a>' +
            '</li>';
    }
    return rtn_html;
}

/**
 * 顯示頁面轉導提醒視窗
 * @param title
 * @param redirect_page
 */
function showRedirectSwal(title, redirect_page) {
    //  sweetAlert彈出視窗設定
    let reminder_swal_config = Object.assign({}, swal_toast_config);
    reminder_swal_config.showCancelButton = true;
    reminder_swal_config.confirmButtonText = '前往頁面<i class="fas fa-angle-right ml-1"></i>';
    let reminder_swal = Swal.mixin(reminder_swal_config);

    reminder_swal.fire({
        title: title
    }).then((result) => {
        if (result.isConfirmed) {
            $('#' + redirect_page).click();
        }
    });
}

/**
 * 產生標籤多選下拉選單
 * @param {string} element_id 要設定的下拉選單id
 * @param {string} root_hall_name 要設定的標籤根廳別
 * @param {string} hall_name 要設定的標籤廳別
 * @param {number[]} tag_category 要設定的標籤種類(1:一般標籤 2:週次標籤 3:時段標籤)
 * @param {boolean} generate_all_option 是否產生全選及各類別全部的選項
 * @param {boolean} generate_custom_option 是否產生目標族群的選項，否則產生系統預設標籤
 * @param {boolean} is_check_tag_usage 是否檢查標籤可用性
 */
function generateTagMultiSelect(element_id, root_hall_name, hall_name, tag_category = [1], generate_all_option = false, generate_custom_option = false, is_check_tag_usage = true) {
    let tag_option;
    let value_ary = $('#' + element_id).val();  // 記錄目前下拉選單選取的項目
    $('#' + element_id).find('option').remove();  //  將全部的option移除

    if(generate_custom_option){
        let custom_opt_dict = getSessionStorageEntity('system_config').custom_tags;

        if(generate_all_option){
            tag_option = new Option('全部', 'all', false, false);
            $('#' + element_id).append(tag_option);
        }

        $.each(custom_opt_dict, function (key, value) {
            if(hall_name == findHallIdMappingKey([root_hall_name], {hall_id: value.hall_id, domain_id: value.domain_id})){
                tag_option = new Option(value.tags_name, value.tags_index, false, false);
                $('#' + element_id).append(tag_option);
            }
        });
    }else{
        if(generate_all_option){
            tag_option = new Option('全部', 'all', false, false);
            $('#' + element_id).append(tag_option);
            tag_option = new Option('全部【人工】標籤', 'all_1', false, false);
            $('#' + element_id).append(tag_option);
            tag_option = new Option('全部【規則】標籤', 'all_3', false, false);
            $('#' + element_id).append(tag_option);
            tag_option = new Option('全部【機器】標籤', 'all_4', false, false);
            $('#' + element_id).append(tag_option);
        }

        let tag_sort_dict = {};  // 存放排序好的標籤字典
        let tag_description_dict = getSessionStorageEntity('system_config').tags_config[hall_name];
        $.each(tag_description_dict, function(key,value) {
            //  若需檢查標籤是否禁用且標籤代碼禁用，則跳過不顯示
            if(!checkTagUsage(hall_name, key) && is_check_tag_usage){
                return;  // 相當於continue
            }
            //  判斷標籤種類是否為要設定的種類，若不是則跳過不新增
            if(!(tag_category.includes(value.tag_category))){
                return;  // 相當於continue
            }

            tag_option = new Option(value.tag_name, key, false, false);
            tag_sort_dict[value.sort_index] = tag_option;
        });
        
        //  將排序完成的標籤option貼上select
        $.each(tag_sort_dict, function (key, value) {
            $('#' + element_id).append(value);
        });

        $('#' + element_id).val(value_ary);  //  設定原本選中的選項
    }
}

/**
 * 設定標籤下拉多選選單選項狀態
 * @param {string} element_id 要設定的下拉選單id
 * @param {string} hall_name 要設定的標籤廳別
 * @param {number[]} tag_category 要設定的標籤種類(1:一般標籤 2:週次標籤 3:時段標籤)
 * @param {boolean} is_custom_tags_option 是否使用目標族群的option
 * @return {string} 標籤代碼字串(逗號分隔)
 */
function settingTagMultiSelectOptionStatus(element_id, hall_name, tag_category = [1], is_custom_tags_option = false) {
    let o = $('#' + element_id + ' option');
    let all_dict = {};

    for(let i=0 ; i<o.length ; i++){
        $(o[i]).prop('disabled', false);  // 將選項全部先取消disabled
        if(o[i].selected){  // 紀錄目前被選取的選項
            all_dict[o[i].value] += o[i].value;
        }
    }

    if(o[0].selected && o[0].value === 'all'){
        for(let i=0 ; i<o.length ; i++){
            if(o[i].value !== 'all'){
                $(o[i]).prop('disabled', true);  // 若選中'全部'，將其他選項disabled
                delete all_dict[o[i].value];
            }
        }
    }
    if(!is_custom_tags_option){
        if(o[1].selected && o[1].value === 'all_1'){
            for(let i=0 ; i<o.length ; i++){
                if(o[i].value !== 'all_1'){
                    if(o[i].value.substring(0, 1) === '1'){
                        $(o[i]).prop('disabled', true);  // 若選中'全部【人工】標籤'，將【人工】標籤選項disabled
                        delete all_dict[o[i].value];
                    }
                }
            }
        }
        if(o[2].selected && o[2].value === 'all_3'){
            for(let i=0 ; i<o.length ; i++){
                if(o[i].value !== 'all_3'){
                    if(o[i].value.substring(0, 1) === '3'){
                        $(o[i]).prop('disabled', true);  // 若選中'全部【規則】標籤'，將【規則】標籤選項disabled
                        delete all_dict[o[i].value];
                    }
                }
            }
        }
        if(o[3].selected && o[3].value === 'all_4'){
            for(let i=0 ; i<o.length ; i++){
                if(o[i].value !== 'all_4'){
                    if(o[i].value.substring(0, 1) === '4'){
                        $(o[i]).prop('disabled', true);  // 若選中'全部【機器】標籤'，將【機器】標籤選項disabled
                        delete all_dict[o[i].value];
                    }
                }
            }
        }
    }

    //  處理要發查api的隱藏標籤字串
    let all_ary = [];
    let hide_value = '';  // 紀錄發查後端的標籤字串
    $.each(all_dict, function(key,value) {
        if(is_custom_tags_option){
            if(key === 'all'){
                for(let i=1 ; i<o.length ; i++){
                    hide_value += o[i].value + ',';
                }
            }else{
                hide_value += key + ',';
            }
        }else{
            let tag_description_dict = getSessionStorageEntity('system_config').tags_config[hall_name];
            if(key === 'all'){
                $.each(tag_description_dict, function(key,value) {
                    //  若標籤代碼禁用，則跳過不顯示
                    if(!checkTagUsage(hall_name, key)){
                        return;  // 相當於continue
                    }
                    //  判斷標籤種類是否為要設定的種類，若不是則跳過不新增至回傳的標籤字串中
                    if(!(tag_category.includes(value.tag_category))){
                        return;  // 相當於continue
                    }
                    hide_value += key + ',';
                });
            }else if(key === 'all_1'){
                $.each(tag_description_dict, function(key,value) {
                    if(key.substring(0, 1) === '1'){
                        //  若標籤代碼禁用，則跳過不顯示
                        if(!checkTagUsage(hall_name, key)){
                            return;  // 相當於continue
                        }
                        //  判斷標籤種類是否為要設定的種類，若不是則跳過不新增至回傳的標籤字串中
                        if(!(tag_category.includes(value.tag_category))){
                            return;  // 相當於continue
                        }
                        hide_value += key + ',';
                    }
                });
            }else if(key === 'all_3'){
                $.each(tag_description_dict, function(key,value) {
                    if(key.substring(0, 1) === '3'){
                        //  若標籤代碼禁用，則跳過不顯示
                        if(!checkTagUsage(hall_name, key)){
                            return;  // 相當於continue
                        }
                        //  判斷標籤種類是否為要設定的種類，若不是則跳過不新增至回傳的標籤字串中
                        if(!(tag_category.includes(value.tag_category))){
                            return;  // 相當於continue
                        }
                        hide_value += key + ',';
                    }
                });
            }else if(key === 'all_4'){
                $.each(tag_description_dict, function(key,value) {
                    if(key.substring(0, 1) === '4'){
                        //  若標籤代碼禁用，則跳過不顯示
                        if(!checkTagUsage(hall_name, key)){
                            return;  // 相當於continue
                        }
                        //  判斷標籤種類是否為要設定的種類，若不是則跳過不新增至回傳的標籤字串中
                        if(!(tag_category.includes(value.tag_category))){
                            return;  // 相當於continue
                        }
                        hide_value += key + ',';
                    }
                });
            }else{
                //  若標籤代碼禁用，則跳過不顯示
                if(!checkTagUsage(hall_name, key)){
                    return;  // 相當於continue
                }
                //  判斷標籤種類是否為要設定的種類，若不是則跳過不新增至回傳的標籤字串中
                if(!(tag_category.includes(value.tag_category))){
                    return;  // 相當於continue
                }
                hide_value += key + ',';
            }
        }

        all_ary.push(key);  // 將字典轉為陣列賦值給下拉多選單
    });

    $('#' + element_id).val(all_ary);
    hide_value = hide_value.substr(0, hide_value.length - 1); //去掉末尾的逗號

    return hide_value;
}

/**
 * 設定目標族群可選廳別狀態
 * @param source_page
 * @param element
 */
function setCustomTagsHallSelectOption(source_page, element) {
    let hall_name = $(element).prev().val();  // 紀錄目前checkbox的廳別
    let custom_tags_hall_select_el_ary = $('#' + source_page + '_custom_tags_div').find('.hall_select2');
    for(let j=0 ; j<custom_tags_hall_select_el_ary.length ; j++){
        let hall_select_el = $(custom_tags_hall_select_el_ary[j]);
        let hall_select_opt_el = $(custom_tags_hall_select_el_ary[j]).find('option[value="' + hall_name + '"]');
        if($(element).prev().is(':checked')){
            hall_select_opt_el.prop('disabled', true);  // disabled option
            if(hall_select_el.find('option:selected').val() === hall_name){
                if(!($('#' + source_page + '_custom_tags_div').hasClass('show'))){
                    $('#'+ source_page + '_custom_tags_switch').click();  // 將目標族群section展開
                }
                hall_select_el.closest('.custom_tags_row').find('.tags_edit_btn').click();  // 啟用有選中該廳別的目標族群row
                hall_select_el.closest('.custom_tags_row').find('.tags_delete_btn').click();  // 將該row刪除
            }
        }else{
            hall_select_opt_el.prop('disabled', false);  // enabled option
        }
    }
}

/**
 * 產生目標族群組合的輸入框
 * @param source_page {string} 依據來源page決定廳別下拉選單是否套用權限(user開頭:套用 admin開頭:不套用)
 * @param ul_id 要放置的ul元件id
 * @param {string[]} tags_data 要顯示的資料
 */
function generateCustomTagsInputHtml(source_page = 'user_detail_info', ul_id, tags_data = null) {
    let element = $('#' + ul_id);  // 取得element selector
    let custom_tags_name_ary = $(element).find('input[id*="custom_tags_name"]');
    let exist_custom_tags_index_map = {};
    let tags_input_index = null;

    //  產生目前已存在的index字典
    for(let i=0 ; i<custom_tags_name_ary.length ; i++){
        let current_tags_id_ary = $(custom_tags_name_ary[i])[0].id.split('_');
        let current_index = current_tags_id_ary[current_tags_id_ary.length - 1];
        exist_custom_tags_index_map[current_index] = current_index;
    }

    //  取得此次要使用的index
    if(tags_data !== null){
        tags_input_index = tags_data.tags_index;
    }else{
        for(let i=0 ; i<custom_tags_limit ; i++){
            if(exist_custom_tags_index_map[i+1] === undefined){
                tags_input_index = i+1;
                //  判斷新增的index在隱藏的刪除input是否啟用，若啟用則取消
                if($('#' + source_page + '_delete_custom_tags_' + tags_input_index).prop('disabled') === false){
                    $('#' + source_page + '_delete_custom_tags_' + tags_input_index).removeClass('enabled').prop('disabled', true);
                }
                break;
            }
        }
    }

    //  判斷目前目標族群的數量是否大於設定值
    if(tags_input_index <= custom_tags_limit && tags_input_index !== null){
        let rtn_html = '<li class="custom_tags_li">' +
            '<div class="row custom_tags_row">' +
            '<div class="col-md-auto custom_tags_col">' +
            '<span class="mt-2 handle">' +
            '<i class="fas fa-ellipsis-v mr-1"></i>' +
            '<i class="fas fa-ellipsis-v"></i>' +
            '</span>' +
            '</div>' +
            '<div class="col-md-2 custom_tags_col">' +
            '<div class="input-group">' +
            '<select class="select2 hall_select2" id="' + source_page + '_custom_tags_hall_name_' + tags_input_index + '" name="custom_tags_hall_name_' + tags_input_index + '" data-placeholder="請選擇廳別" style="width: 100%;">' +
            '<option></option>' +
            '</select>' +
            '</div>' +
            '</div>' +
            '<div class="col-md-2 custom_tags_col">' +
            '<div class="input-group">' +
            '<input class="form-control tags_name_input" id="' + source_page + '_custom_tags_name_' + tags_input_index + '" name="custom_tags_name_' + tags_input_index + '" type="text" placeholder="輸入目標族群名稱">' +
            '</div>' +
            '</div>' +
            '<div class="col-md-7 custom_tags_col">' +
            '<div class="input-group">' +
            '<div class="select2-primary" style="width: 100%;">' +
            '<select class="select2 tags_select2" id="' + source_page + '_custom_tags_' + tags_input_index + '" name="custom_tags_' + tags_input_index + '[]" multiple="multiple" data-placeholder="選擇包含標籤..." data-dropdown-css-class="select2-primary" style="width: 100%;">' +
            '</select>' +
            '</div>' +
            '<img src="./dist/img/loading.gif" class="img-size-36 m-auto loading-icon" style="display: none;">' +
            '<input class="tags_hide_input" type="hidden" id="' + source_page + '_custom_tags_hide_' + tags_input_index + '" name="custom_tags_hide_' + tags_input_index + '" value="">' +
            '</div>' +
            '</div>' +
            '<div class="col-md-auto custom_tags_col operate_col">' +
            '<button class="btn btn-block btn-tool mt-auto text-lg tags_edit_btn" id="' + source_page + '_custom_tags_edit_' + tags_input_index + '" type="button" style="display: none;"><i class="fas fa-edit"></i></button>' +
            '<button class="btn btn-block btn-tool mt-auto text-lg tags_delete_btn" id="' + source_page + '_custom_tags_delete_' + tags_input_index + '" type="button" style="display: none;"><i class="fas fa-times"></i></button>' +
            '</div>' +
            '</div>' +
            '</li>';
        $('#'+ source_page + '_custom_tags_ul').append(rtn_html);

        let hall_select_element = $(element).find('#' + source_page + '_custom_tags_hall_name_' + tags_input_index);
        let tags_name_element = $(element).find('#' + source_page + '_custom_tags_name_' + tags_input_index);
        let tags_select_element = $(element).find('#' + source_page + '_custom_tags_' + tags_input_index);
        let tags_hide_element = $(element).find('#' + source_page + '_custom_tags_hide_' + tags_input_index);
        let delete_btn_element = $(element).find('#' + source_page + '_custom_tags_delete_' + tags_input_index);


        // 初始化廳別下拉選單，依據來源page決定下拉廳別選項是否只顯示有權限的廳別
        if(source_page === 'admin_user_add' || source_page === 'admin_user_edit'){  // 來自admin頁面
            setHallDropdownList(source_page + '_custom_tags_hall_name_' + tags_input_index, ['BBIN', 'XBB'], false);
            let checkbox_el_ary = $('#' + source_page + '_access_hall_checkbox_div').find('input[type=checkbox]');
            for(let i=0 ; i<checkbox_el_ary.length ; i++){
                if(!($(checkbox_el_ary[i]).is(':checked'))){
                    let disabled_hall_name = $(checkbox_el_ary[i]).val();
                    $('#' + source_page + '_custom_tags_hall_name_' + tags_input_index).find('option[value="' + disabled_hall_name + '"]').prop('disabled', true);
                }
            }
        }else{
            setHallDropdownList(source_page + '_custom_tags_hall_name_' + tags_input_index, ['BBIN', 'XBB'], true);
        }

        if(tags_data !== null){
            let hall_name = findHallIdMappingKey(['BBIN','XBB'],{hall_id: tags_data.hall_id, domain_id: tags_data.domain_id});
            hall_select_element.val(hall_name);
            hall_select_element.prop('disabled', true);

            tags_name_element.val(tags_data.tags_name);
            tags_name_element.prop('disabled', true);

            // 目標族群下拉選單初始化
            generateTagMultiSelect(source_page + '_custom_tags_' + tags_input_index, findRootHall($(hall_select_element).val()), $(hall_select_element).val());

            let tags_select_ary = tags_data.tags_str.split(',');
            tags_select_element.val(tags_select_ary);
            tags_select_element.prop('disabled', true);

            tags_hide_element.val(tags_data.tags_str);
            tags_hide_element.prop('disabled', true);

            //  加上刪除的隱藏input，預設為disabled
            let delete_index_html = '<input type="hidden" class="delete_tags" id="' + source_page + '_delete_custom_tags_' + tags_input_index +
                '" name="delete_custom_tags_' + tags_input_index +
                '" value="【' + hall_config_dict[findRootHall(hall_name)][hall_name].hall_name + '】' + tags_data.tags_name + '" disabled>';
            $('#' + source_page + '_custom_tags_div').append(delete_index_html);
        }else{
            delete_btn_element.parent().parent().parent().addClass('enabled');
            delete_btn_element.attr('onclick', 'deleteCustomTagsInputHtml(\'' + source_page + '\', this);');
            delete_btn_element.show();
        }

        // init jquery storable
        $('#' + source_page + '_custom_tags_ul').sortable({
            placeholder         : 'sort-highlight',
            handle: ".handle",
            forcePlaceholderSize: true,
            zIndex              : 999999
        });

        // Initialize Select2 Elements
        $(hall_select_element).select2({
            minimumResultsForSearch: -1,
            dropdownParent: ($('#' + source_page + '_modal').length !== 0 ? $('#' + source_page + '_modal') : $(document.body))
        });
        $(tags_select_element).select2({
            dropdownParent: ($('#' + source_page + '_modal').length !== 0 ? $('#' + source_page + '_modal') : $(document.body))
        });

        // add jquery validator
        $(hall_select_element).rules('add', {
            required: true,
            messages: {
                required: "<i class=\"icon fas fa-exclamation-triangle mr-1\"></i>請選擇廳別",
            }
        });

        $(tags_name_element).rules('add', {
            required: true,
            maxlength: 20,
            checkContentSpace: true,
            messages: {
                required: "<i class=\"icon fas fa-exclamation-triangle mr-1\"></i>請輸入目標族群名稱",
                maxlength: "<i class=\"icon fas fa-exclamation-triangle mr-1\"></i>名稱過長(最多為20個字元)",
            }
        });

        $(tags_select_element).rules('add', {
            required: true,
            messages: {
                required: "<i class=\"icon fas fa-exclamation-triangle mr-1\"></i>請選擇至少一個標籤"
            }
        });

        //  綁定廳別下拉選單事件，依據所選廳別帶入不同的標籤config
        $(hall_select_element).bind('change', function(){
            let selected_hall_name = $(this).val();
            let loading_icon_element = $(tags_select_element).parent().parent().find('.loading-icon');
            $(loading_icon_element).show();

            $(tags_select_element).next().hide();  // 將select2-container隱藏
            $(tags_select_element).find('option').remove();  //  將全部的option移除

            //  依據所選廳別重新加入option
            generateTagMultiSelect(source_page + '_custom_tags_' + tags_input_index, findRootHall(selected_hall_name), selected_hall_name);

            $(tags_select_element).next().show();  // 將select2-container顯示
            $(loading_icon_element).hide();
        });

        //  綁定事件賦值給隱藏的input
        $(tags_select_element).bind('change',function(event){
            let o = $(tags_select_element).find('option');
            let all = '';
            for(let i=0 ; i<o.length ; i++){
                if(o[i].selected){
                    all += o[i].value+",";
                }
            }
            all = all.substr(0, all.length - 1); //去掉末尾的逗號
            $(tags_hide_element).val(all);//賦值給隱藏的文字框
        });

        //  若新增的數量已達上限，將新增按鈕disabled
        if(custom_tags_name_ary.length + 1 === custom_tags_limit){
            $('#' + source_page + '_add_custom_tags_btn').prop('disabled', true);
            $('#' + source_page + '_add_custom_tags_btn').removeAttr('onclick');
        }
    }
}

/***
 * 啟用目標族群組合的輸入框
 * @param source_page {string} 依據來源page決定廳別下拉選單是否套用權限(user開頭:套用 admin開頭:不套用)
 * @param element 點擊的html元件
 */
function enableChangeCustomTags(source_page, element) {
    $(element).hide();
    $(element).removeAttr('onclick');
    $(element).parent().parent().parent().addClass('enabled');

    $(element).parent().parent().find('.hall_select2').prop('disabled', false);
    $(element).parent().parent().find('.tags_name_input').prop('disabled', false);
    $(element).parent().parent().find('.tags_select2').prop('disabled', false);
    $(element).parent().parent().find('.tags_hide_input').prop('disabled', false);
    $(element).parent().parent().find('.tags_delete_btn').show();
    $(element).parent().parent().find('.tags_delete_btn').attr('onclick', 'deleteCustomTagsInputHtml(\'' + source_page + '\', this);');
}

/***
 * 刪除目標族群組合的輸入框
 * @param source_page {string} 依據來源page決定廳別下拉選單是否套用權限(user開頭:套用 admin開頭:不套用)
 * @param element 點擊的html元件
 */
function deleteCustomTagsInputHtml(source_page, element) {
    let add_tags_btn_element = $('#' + source_page + '_custom_tags_div').find('#' + source_page + '_add_custom_tags_btn');

    let delete_tags_id_ary = $(element)[0].id.split('_');
    let delete_index = delete_tags_id_ary[delete_tags_id_ary.length - 1];
    $('#' + source_page + '_delete_custom_tags_' + delete_index).addClass('enabled').prop('disabled', false);  //  啟用刪除的隱藏input

    $(element).parent().parent().parent().remove();

    add_tags_btn_element.attr('onclick', 'generateCustomTagsInputHtml(\'' + source_page + '\', \'' + source_page + '_custom_tags_div\')');
    add_tags_btn_element.prop('disabled', false);
}

/**
 * 設定進階篩選開啟對應的選單
 * @param advanced_filter_el
 * @param filter_id
 */
function change_advanced_filter_href(advanced_filter_el, filter_id = null) {
    $('.cdp-filter__content.collapse, .filter__form__content.collapse, .date__filter__form__content.collapse').collapse('hide');  // 將篩選下拉選單收合

    if(filter_id){
        $(advanced_filter_el).prop('href', '#' + filter_id + '_advanced-filter');
        $(advanced_filter_el).prop('aria-controls', filter_id + '_advanced-filter');
    }
}

/**
 * 產生collapse卡片展開/收合按鈕
 * @param append_el_id
 * @param target_el_id
 */
function generate_collapse_arrow_btn(append_el_id, target_el_id){
    let collapse_arrow_btn_html = '<div class="cdp-flex-center cdp-outline__cultured cdp__text__glaucous w-100 mt__10" id="' + target_el_id + '_btn" data-toggle="collapse" data-target="#' + target_el_id + '" aria-expanded="false" aria-controls="' + target_el_id + '">' +
        '展開' +
        '<i class="fas fa-caret-down ml__6 d-block"></i>' +
        '</div>';
    $('#' + append_el_id).append(collapse_arrow_btn_html);

    // event binding
    $('#' + target_el_id).on('show.bs.collapse', function () {
        $('#' + target_el_id + '_btn').html('收合<i class="fas fa-caret-up ml__6 d-block"></i>');
    });
    $('#' + target_el_id).on('hide.bs.collapse', function () {
        $('#' + target_el_id + '_btn').html('展開<i class="fas fa-caret-down ml__6 d-block"></i>');
    });
}

/**
 * 初始化標籤欄位的展開/收合按鈕
 * @param element_id
 * @param is_data_table
 */
function initTagsShowMore(element_id, is_data_table = false) {
    let labelListAry = (is_data_table ? $('#' + element_id).dataTable().$('.list-inline') : $('#' + element_id + ' .list-inline'));
    for (let i = 0 ; i < labelListAry.length ; i++) {
        let liItemArray = $(labelListAry[i]).find('li');
        generateTagsOpenAndCloseBtn($(labelListAry[i]), liItemArray);
    }
}

/**
 * 產生標籤展開/收合按鈕
 * @param ulList
 * @param liList
 */
function generateTagsOpenAndCloseBtn(ulList, liList) {
    let eachLabelMaxItem = 6;  // 預設要顯示的標籤數量
    $.each(liList, function (key, value) {
        if ((key+1) === eachLabelMaxItem && eachLabelMaxItem < liList.length) {
            let open_btn = '<button class="cdp-show-more-label" onclick="showAllTags(this);">。。。</button>';
            $(ulList).append(open_btn);
        }else if((key+1) > eachLabelMaxItem){
            $(value).addClass('cdp-list-hide');
        }
    });

    let close_btn = '<button class="cdp-close-label-button hide" onclick="closeAllTags(this);">close</button>';
    $(ulList).append(close_btn);
}

/**
 * 展開標籤
 * @param element
 */
function showAllTags(element) {
    let closeAllBtn = $(element).parent().find('.cdp-close-label-button');
    let hideArray = $(element).parent().find('.cdp-list-hide');
    $.each(hideArray, function (key, value) {
        $(value).hasClass('cdp-list-hide') ? $(value).removeClass('cdp-list-hide') : null;
    });
    $(closeAllBtn).hasClass('hide') ? $(closeAllBtn).removeClass('hide') : null;
    $(element).addClass('hide');
    $(element).parent().addClass('show');
}

/**
 * 收合標籤
 * @param element
 */
function closeAllTags(element) {
    let ulList = $(element).parent();
    let liItemArray = $(ulList).find('li');
    let buttonArray = $(element).parent().find('button');
    $.each(buttonArray, function (key, value) {
        $(value).remove();
    });
    generateTagsOpenAndCloseBtn(ulList, liItemArray);
}