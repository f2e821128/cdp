//  Toastr Init
const toastr_config = {
  positionClass: "toast-top-right",
  progressBar: true,
  showDuration: "300",
  hideDuration: "300",
  timeOut: 0,
  extendedTimeOut: "1000",
  showMethod: "slideDown",
  hideMethod: "slideUp",
};
