const dictionary = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

const dataSet = [
  {
    name: "Airi Satou",
    position: "Accountant",
    office: "Tokyo",
    age: "33",
    start_date: "2008/11/28",
    salary: "$162,700",
  },
  {
    name: "Angelica Ramos",
    position: "Chief Executive Officer (CEO)",
    office: "London",
    age: "47",
    start_date: "2009/10/09",
    salary: "$1,200,000",
  },
  {
    name: "Charlie Kelly",
    position: "Senior Javascript Developer",
    office: "Edinburgh",
    age: "18",
    start_date: "2012/03/29",
    salary: "$433,060",
  },
  {
    name: "Bradley Greer",
    position: "Software Engineer",
    office: "London",
    age: "41",
    start_date: "2012/10/13",
    salary: "$132,700",
  },
  {
    name: "Brielle Williamson",
    position: "Integration Specialist",
    office: "New York",
    age: "61",
    start_date: "2012/12/02",
    salary: "$372,000",
  },
  {
    name: "Cedric Kelly",
    position: "Senior Javascript Developer",
    office: "Edinburgh",
    age: "22",
    start_date: "2012/03/29",
    salary: "$433,060",
  },
  {
    name: "Airi Satou",
    position: "Accountant",
    office: "Tokyo",
    age: "33",
    start_date: "2008/11/28",
    salary: "$162,700",
  },
  {
    name: "Cara Stevens",
    position: "Sales Assistant",
    office: "New York",
    age: "46",
    start_date: "2011/12/06",
    salary: "$145,600",
  },
  {
    name: "Caesar Vance",
    position: "Pre-Sales Support",
    office: "New York",
    age: "21",
    start_date: "2011/12/12",
    salary: "$106,450",
  },
  {
    name: "Bruno Nash",
    position: "Software Engineer",
    office: "London",
    age: "38",
    start_date: "2011/05/03",
    salary: "$163,500",
  },
  {
    name: "Brielle Williamson",
    position: "Integration Specialist",
    office: "New York",
    age: "61",
    start_date: "2012/12/02",
    salary: "$372,000",
  },
  {
    name: "Brenden Wagner",
    position: "Software Engineer",
    office: "San Francisco",
    age: "28",
    start_date: "2011/06/07",
    salary: "$206,850",
  },
  {
    name: "Bradley Greer",
    position: "Software Engineer",
    office: "London",
    age: "41",
    start_date: "2012/10/13",
    salary: "$132,700",
  },
  {
    name: "Ashton Cox",
    position: "Junior Technical Author",
    office: "San Francisco",
    age: "66",
    start_date: "2009/01/12",
    salary: "$86,700",
  },
  {
    name: "Charles Kelly",
    position: "Senior Javascript Developer",
    office: "Edinburgh",
    age: "22",
    start_date: "2012/03/29",
    salary: "$433,060",
  },
];

function uuid() {
  let result = "";
  const max = dictionary.length;
  for (let i = 0; i < 20; i++) {
    result += dictionary[Math.floor(Math.random() * max)];
  }
  return result;
}

function checkboxRender() {
  let checkboxID = uuid();
  return `
        <div class="icheck-primary d-inline table-checkbox">
            <input type="checkbox" id="${checkboxID}"/>
            <label for="${checkboxID}"></label>
        </div>`;
}

function buttonRender(modalID) {
  let buttonID = uuid();
  return `
          <button class="cdp-btn cdp-btn__sm ${modalID === "#review" ? "custom-bg-grey" : "custom-bg-blue"} mr__6" id="${buttonID}" onclick="openModal('${modalID}')">
            ${modalID === "#review" ? '<i class="fas fa-eye mr__6"></i>' : '<i class="custom-icon-edit mr__6"></i>'}
            ${modalID.replace("#", "")}
          </button>`;
}

function tableGenerator({ tableID, checkbox, button, button2, perPageRow, numOfCol, numOfDataSet = 1 }) {
  numOfCol = numOfCol > 6 ? 6 : numOfCol - 1;

  let renderData = [];
  if (numOfDataSet !== 1) {
    for (let i = 0; i < numOfDataSet; i++) {
      for (let j = 0; j < dataSet.length; j++) {
        renderData.push(dataSet[i]);
      }
    }
  } else {
    renderData = dataSet;
  }

  let output = `<tbody>`;
  renderData.forEach((rowObject) => {
    let objectKey = ["name", "position", "office", "age", "start_date"];
    let tdCol = "";
    for (let i = 0; i < numOfCol; i++) {
      tdCol += `<td> ${rowObject[objectKey[i]]} </td>`;
    }

    if (checkbox) output += `<tr>${[tdCol]}<td>${checkboxRender()}</td></tr>`;

    if (button?.status) {
      output += button2?.status ? `<tr>${[tdCol]}<td>${buttonRender(button.modalID)}${buttonRender(button2.modalID)}</td></tr>` : `<tr>${[tdCol]}<td>${buttonRender(button.modalID)}</td></tr>`;
    }

    if (!checkbox && !button?.status) {
      output += `<tr>${[tdCol]}<td>${rowObject.salary}</td></tr>`;
    }
  });
  output += `</tbody>`;

  $(`${tableID}`).append(output);

  $(`${tableID}`).DataTable({
    paging: true,
    pagingType: "simple_numbers",
    pageLength: perPageRow,
    lengthChange: false,
    searching: false,
    ordering: true,
    info: true,
    oLanguage: {
      sZeroRecords: "查無資料",
      sInfo: "_START_ - _END_ / Total _TOTAL_ entries",
      sInfoEmpty: "0 - 0 / Total 0 entries",
      oPaginate: {
        sPrevious: `<svg class="cdpiconalibaba-12" aria-hidden="true"><use xlink:href="#icon-icon_prev"></use></svg>`,
        sNext: `<svg class="cdpiconalibaba-12" aria-hidden="true"><use xlink:href="#icon-icon_next"></use></svg>`,
      },
    },
    autoWidth: false,
    aaSorting: [],
    columnDefs: [
      {
        targets: "sorting_disabled",
        orderable: false,
      },
    ],
    dom: '<<"col-12 p-0"frt>><"card-footer text-sm text-center cdp__footer__pagination"ip>',
  });
}
