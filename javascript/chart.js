function piechartGenerator(id) {
  new Chart(document.getElementById(id), {
    type: "pie",
    data: {
      labels: ["Red", "Blue", "Yellow"],
      datasets: [
        {
          label: "Pie Chart Example",
          data: [300, 50, 100],
          backgroundColor: ["rgb(255, 99, 132)", "rgb(54, 162, 235)", "rgb(255, 205, 86)"],
          hoverOffset: 4,
        },
      ],
    },
    options: {
      responsive: true,
    },
  });
}

function linechartGenerator(id, showLabel) {
  new Chart(document.getElementById(id), {
    type: "line",
    data: {
      labels: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
      datasets: [
        {
          label: "Line 01",
          data: [65, 59, 80, 81, 56, 55, 40, 93, 26, 40, 31, 14],
          fill: false,
          borderColor: "#e8465e",
          backgroundColor: "#e8465e",
          tension: 0.1,
        },
        {
          label: "Line 02",
          data: [51, 67, 84, 13, 47, 1, 82, 0, 37, 50, 80, 37],
          fill: false,
          borderColor: "#135b86",
          backgroundColor: "#135b86",
          tension: 0.1,
        },
        {
          label: "Line 03",
          data: [83, 32, 25, 19, 90, 91, 53, 88, 70, 13, 52, 52],
          fill: false,
          borderColor: "#f9b40c",
          backgroundColor: "#f9b40c",
          tension: 0.1,
        },
        {
          label: "Line 04",
          data: [19, 91, 70, 55, 48, 84, 97, 81, 10, 99, 48, 63],
          fill: false,
          borderColor: "#f36d30",
          backgroundColor: "#f36d30",
          tension: 0.1,
        },
        {
          label: "Line 05",
          data: [64, 35, 71, 35, 29, 50, 80, 96, 26, 29, 48, 36],
          fill: false,
          borderColor: "#922943",
          backgroundColor: "#922943",
          tension: 0.1,
        },
        {
          label: "Line 06",
          data: [75, 5, 7, 73, 8, 91, 93, 42, 33, 31, 90, 26],
          fill: false,
          borderColor: "#3e96a9",
          backgroundColor: "#3e96a9",
          tension: 0.1,
        },
        {
          label: "Line 07",
          data: [1, 49, 75, 80, 29, 19, 5, 60, 44, 92, 88, 9],
          fill: false,
          borderColor: "#94c3c7",
          backgroundColor: "#94c3c7",
          tension: 0.1,
        },
      ],
      hoverOffset: 4
    },
    options: {
      responsive: true,
      plugins: {
        legend: {
          display: showLabel,
        },
      },
    },
  });
}

function barchartGenerator(id) {
  new Chart(document.getElementById(id), {
    type: "bar",
    data: {
      labels: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
      datasets: [
        {
          label: "Bar Chart 01",
          data: [65, 59, 80, 81, 56, 55, 40, 93, 26, 40, 31, 14],
          backgroundColor: ["#DEC1FF", "#343633", "#5CC8FF", "#8AEA92", "#CAC4CE", "#F5FF90", "#D6FFB7", "#FF9F1C", "#FFC15E", "#ECDCB0", "#FFCAB1", "#8CC084"],
          borderColor: ["#DEC1FF", "#343633", "#5CC8FF", "#8AEA92", "#CAC4CE", "#F5FF90", "#D6FFB7", "#FF9F1C", "#FFC15E", "#ECDCB0", "#FFCAB1", "#8CC084"],
          borderWidth: 1,
        },
      ],
    },
    options: {
      responsive: true,
      scales: {
        y: {
          beginAtZero: true,
        },
      },
    },
  });
}

function polarAreaChartGenerator(id) {
  const data = {
    labels: ["Red", "Green", "Yellow", "Grey", "Blue"],
    datasets: [
      {
        label: "My First Dataset",
        data: [11, 16, 7, 3, 14],
        backgroundColor: ["rgb(255, 99, 132)", "rgb(75, 192, 192)", "rgb(255, 205, 86)", "rgb(201, 203, 207)", "rgb(54, 162, 235)"],
      },
    ],
  };

  const config = {
    type: "polarArea",
    data: data,
    options: {
      responsive: true,
    },
  };

  new Chart(document.getElementById(id), config);
}

function doughnutChartGenerator(id) {
  const data = {
    labels: ["Red", "Blue", "Yellow"],
    datasets: [
      {
        label: "My First Dataset",
        data: [300, 50, 100],
        backgroundColor: ["rgb(255, 99, 132)", "rgb(54, 162, 235)", "rgb(255, 205, 86)"],
        hoverOffset: 4,
      },
    ],
  };

  const config = {
    type: "doughnut",
    data: data,
    options: {
      responsive: true,
    },
  };

  new Chart(document.getElementById(id), config);
}
