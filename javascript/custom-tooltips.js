/**
 * Using Jquery listener mouse hover event
 * @params DOM Event
 * Hover event get target & find out closest <a> tag.
 * Using <a> tag attribute "data-target" get child DOM
 */
$('[data-toggle="tooltip-2"]').mouseover(function (event) {
  const target = event.target.closest("a");
  const tooltip = target.getAttribute("data-target");
  $(`#${tooltip}`).removeClass("d-none");
  $(`#${tooltip}`).addClass("d-block");
  $(`#${tooltip}`).addClass("position-absolute");
  $(`#${tooltip}`).addClass("mt-2");
});

$('[data-toggle="tooltip-2"]').mouseleave(function (event) {
  const target = event.target.closest("a");
  const tooltip = target.getAttribute("data-target");
  $(`#${tooltip}`).removeClass("d-block");
  $(`#${tooltip}`).removeClass("position-absolute");
  $(`#${tooltip}`).removeClass("mt-2");
  $(`#${tooltip}`).addClass("d-none");
});
