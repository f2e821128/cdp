function openModal(modalID) {
  $("body").addClass("modal-open");
  $(`${modalID}`).addClass("fade show");
  $(`${modalID}`).css("display", "block");
  $(`${modalID}`).css("background-color", "rgba(0, 0, 0, 0.5)");
}

function closeModal(modalID) {
  $("body").removeClass("modal-open");
  $(`${modalID}`).removeClass("fade show");
  $(`${modalID}`).css("display", "none");
  $(`${modalID}`).css("background-color", "");
}
