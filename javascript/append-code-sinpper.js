function appendCodeSinpper(id, codeStr) {
  codeStr = codeStr.replaceAll("/n", "<br/>");
  codeStr = codeStr.replaceAll("/tab", " &nbsp;");
  document.querySelector(id).insertAdjacentHTML("beforeend", codeStr);
}
