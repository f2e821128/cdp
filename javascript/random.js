function cdpRandomNumberFunc(range) {
  return Math.floor(Math.random() * range);
}

const cdpRandomNumberArrowFunc = (range) => {
  return Math.floor(Math.random() * range);
};
