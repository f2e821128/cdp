const bad = "#f16565";
const normal = "#F8DE1B";
const good = "#0bb000";

const cdpCOLOUR = [bad, normal, good];

function cdpColourFunc(range) {
  return cdpCOLOUR[range == 3 ? range - 1 : range];
}

const cdpColourArrowFunc = (range) => {
  return cdpCOLOUR[range == 3 ? range - 1 : range];
};
